package function;

import electronic_store.*;
import cexception.*;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;

/**
 * Unit tests for the StorageManager class.
 */
public class StorageManagerTest {
    Storage storage = new FileHandler();
    StorageManager manager = new StorageManager(storage);

    // Test for modifying the name of an electronic item.
    @Test
    public void testModifyName() throws DataException {

        Computer computer = createMockComputer();
        storage.electronicList.add(computer);

        manager.modifyName("ID123", "New Model");

        assertEquals("New Model", computer.getName());
    }
	// Test for modifying the brand of an electronic item.
	    @Test
	    public void testModifyBrand() throws DataException {

	        Computer computer = createMockComputer();
	        storage.electronicList.add(computer);

	        manager.modifyBrand("ID123", "New Brand");

	        assertEquals("New Brand", computer.getBrand());
	    }

	    // Test for modifying the category of an electronic item.
	    @Test
	    public void testModifyCategory() throws DataException {

	        Computer computer = createMockComputer();
	        storage.electronicList.add(computer);

	        manager.modifyCategory("ID123", "Desktop");

	        assertEquals("Desktop", computer.getCategory());
	    }

	    // Test for modifying the production year of an electronic item.
	    @Test
	    public void testModifyProductionYear() throws DataException {

	        Computer computer = createMockComputer();
	        storage.electronicList.add(computer);

	        manager.modifyProductionYear("ID123", 2025);

	        assertEquals(2025, computer.getProductionYear());
	    }

	    // Test for modifying the warranty period of an electronic item.
	    @Test
	    public void testModifyWarranty() throws DataException {

	        Computer computer = createMockComputer();
	        storage.electronicList.add(computer);

	        manager.modifyWarranty("ID123", 5);

	        assertEquals(5, computer.getWarranty());
	    }

	    // Test for modifying the price of an electronic item.
	    @Test
	    public void testModifyPrice() throws DataException {

	        Computer computer = createMockComputer();
	        storage.electronicList.add(computer);

	        manager.modifyPrice("ID123", 2000.00);

	        assertEquals(2000.00, computer.getPrice(), 0.0);
	    }

	    // Test for modifying the description of an electronic item.
	    @Test
	    public void testModifyDescription() throws DataException {

	        Computer computer = createMockComputer();
	        storage.electronicList.add(computer);

	        manager.modifyDescription("ID123", "Updated Description");

	        assertEquals("Updated Description", computer.getDescription());
	    }

	    // Test for modifying the storage capacity of an electronic item.
	    @Test
	    public void testModifyStorage() throws DataException {

	        Computer computer = createMockComputer();
	        storage.electronicList.add(computer);

	        manager.modifyStorage("ID123", 1024);

	        assertEquals(1024, computer.getStorage(), 0.0);
	    }

	    // Test for modifying the RAM capacity of an electronic item.
	    @Test
	    public void testModifyRam() throws DataException {

	        Computer computer = createMockComputer();
	        storage.electronicList.add(computer);

	        manager.modifyRam("ID123", 32);

	        assertEquals(32, computer.getRam(), 0.0);
	    }
	    // Test for modifying the CPU of a computer.
	     @Test
	     public void testModifyCPU() throws DataException {

	         Computer computer = createMockComputer();
	         storage.electronicList.add(computer);

	         manager.modifyCPU("ID123", "AMD Ryzen");

	         assertEquals("AMD Ryzen", computer.getCPU());
	     }

	     // Test for modifying the operating system of a computer.
	     @Test
	     public void testModifyCSystem() throws DataException {

	         Computer computer = createMockComputer();
	         storage.electronicList.add(computer);

	         manager.modifyCSystem("ID123", "Linux");

	         assertEquals("Linux", computer.getSystem());
	     }

	     // Test for modifying the graphics card of a computer.
	     @Test
	     public void testModifyCGraphicCard() throws DataException {

	         Computer computer = createMockComputer();
	         storage.electronicList.add(computer);

	         manager.modifyCGraphicCard("ID123", "AMD Radeon");

	         assertEquals("AMD Radeon", computer.getGraphicCard());
	     }

	     // Test for modifying the portability of a computer.
	     @Test
	     public void testModifyCPortable() throws DataException {

	         Computer computer = createMockComputer();
	         storage.electronicList.add(computer);

	         manager.modifyCPortable("ID123", false);

	         assertFalse(computer.isPortable());
	     }

	     // Test for modifying the SCPU of a server.
	     @Test
	     public void testModifySSCPU() throws DataException {

	         Server server = createMockServer();
	         storage.electronicList.add(server);

	         manager.modifySSCPU("ID123", "Intel Xeon");

	         assertEquals("Intel Xeon", server.getSCPU());
	     }

	     // Test for modifying the storage system of a server.
	     @Test
	     public void testModifySStorageSystem() throws DataException {

	         Server server = createMockServer();
	         storage.electronicList.add(server);

	         manager.modifySStorageSystem("ID123", "HDD");

	         assertEquals("HDD", server.getStorageSystem());
	     }

	     // Test for modifying the calculation GPU of a server.
	     @Test
	     public void testModifySCalculationGPU() throws DataException {

	         Server server = createMockServer();
	         storage.electronicList.add(server);

	         manager.modifySCalculationGPU("ID123", "NVIDIA Tesla");

	         assertEquals("NVIDIA Tesla", server.getCalculationGPU());
	     }

	     // Test for modifying the port of a server.
	     @Test
	     public void testModifySPort() throws DataException {

	         Server server = createMockServer();
	         storage.electronicList.add(server);

	         manager.modifySPort("ID123", "WiFi");

	         assertEquals("WiFi", server.getPort());
	     }

	     // Test for modifying the bandwidth of a server.
	     @Test
	     public void testModifySBandwidth() throws DataException {

	         Server server = createMockServer();
	         storage.electronicList.add(server);

	         manager.modifySBandwidth("ID123", 2000);

	         assertEquals(2000, server.getBandwidth(), 0.0);
	     }

	     // Test for modifying the raid of a server.
	     @Test
	     public void testModifySRaid() throws DataException {

	         Server server = createMockServer();
	         storage.electronicList.add(server);

	         manager.modifySRaid("ID123", "RAID 1");

	         assertEquals("RAID 1", server.getRaid());
	     }
		 @Test
		 public void testModifyCName() throws DataException {

		     Customer customer = createMockCustomer();
		     storage.customerList.add(customer);

		     manager.modifyCName("ID123", "Jane Doe");

		     assertEquals("Jane Doe", customer.getName());
		 }

		 // Test for modifying the age of a customer.
		 @Test
		 public void testModifyCAge() throws DataException {

		     Customer customer = createMockCustomer();
		     storage.customerList.add(customer);

		     manager.modifyCAge("ID123", 35);

		     assertEquals(35, customer.getAge());
		 }

		 // Test for modifying the email of a customer.
		 @Test
		 public void testModifyCEmail() throws DataException {

		     Customer customer = createMockCustomer();
		     storage.customerList.add(customer);

		     manager.modifyCEmail("ID123", "jane.doe@example.com");

		     assertEquals("jane.doe@example.com", customer.getEmail());
		 }

		 // Test for modifying the address of a customer.
		 @Test
		 public void testModifyCAddress() throws DataException {

		     Customer customer = createMockCustomer();
		     storage.customerList.add(customer);

		     manager.modifyCAddress("ID123", "456 Elm Street");

		     assertEquals("456 Elm Street", customer.getAddress());
		 }

		 // Test for deleting a product.
		 @Test
		 public void testDeleteProduct() throws DataException {

		     Computer computer = createMockComputer();
		     storage.electronicList.add(computer);

		     manager.deleteProduct("ID123");

		     assertTrue(storage.electronicList.isEmpty());
		 }

		 // Test for deleting a customer.
		 @Test
		 public void testDeleteCustomer() throws DataException {

		     Customer customer = createMockCustomer();
		     storage.customerList.add(customer);

		     manager.deleteCustomer("ID123");

		     assertTrue(storage.customerList.isEmpty());
		 }

		 // Test for deleting an order.
		 @Test
		 public void testDeleteOrder() throws DataException {

		     Order order = createMockOrder();
		     storage.orderList.add(order);

		     manager.deleteOrder("OrderID123");

		     assertTrue(storage.orderList.isEmpty());
		 }

		 private Customer createMockCustomer() {
		     return new Customer("John Doe", "ID123", 30, "john.doe@example.com", "123 Main St");
		 }
		 private Order createMockOrder() {
		     return new Order("OrderID123", "CustomerID123", "ProductID123", 5, 1000.00);
		 }
		 private Computer createMockComputer() {
		     return new Computer("ModeWl X", "ID123", "BrandY", "Laptop", 2023, 2, 1500.00,
		                         "High-end gaming laptop", 512, 16, 10, "Intel Core i7",
		                         "Windows 10", "NVIDIA GTX 1080", true);
		 }
		 private Server createMockServer() {
		     return new Server("Server Model", "ID123", "BrandZ", "Server", 2022, 3, 3000.00,
		                       "High-performance server", 2048, 64, 5, "AMD Ryzen",
		                       "SSD", "NVIDIA Tesla", "Ethernet", 1000, "RAID 5");
		 }
		 }