package comparer;

import electronic_store.*;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for the NameComparer class.
 */
public class PriceComparerTest{

    /**
     * Method generate a sample computer
     * @param price
     * @return Computer
     */
    private Computer setPrice(double price) {
        return new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, price, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
    }
    /**
     * Test the comparison when r is true (natural order based on price).
     */
    @Test
    public void testCompareElectronicsWithRTrue() {
        PriceComparer comparer = new PriceComparer();
        Electronics e1 = setPrice(100.0);
        Electronics e2 = setPrice(200.0);

        int result = comparer.compareElectronics(e1, e2, true);

        assertTrue("Comparison with r=true should favor lower price of e1", result < 0);
    }

    /**
     * Test the comparison when r is false (reverse order based on price).
     */
    @Test
    public void testCompareElectronicsWithRFalse() {
        PriceComparer comparer = new PriceComparer();
        Electronics e1 = setPrice(100.0);
        Electronics e2 = setPrice(200.0);

        int result = comparer.compareElectronics(e1, e2, false);

        assertTrue("Comparison with r=false should favor higher price of e2", result > 0);
    }
}






