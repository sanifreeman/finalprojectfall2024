package comparer;

import electronic_store.*;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for the RamComparer class.
 */
public class RamComparerTest {

    /**
     * Method generate a sample computer
     * @param ram
     * @return Computer
     */
    private Computer setRam(double ram) {
        return new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, ram,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
    }

    /**
     * Test the comparison when r is true (natural order based on RAM).
     */
    @Test
    public void testCompareElectronicsWithRTrue() {
        RamComparer comparer = new RamComparer();
        Electronics e1 = setRam(8);  // 8 GB RAM
        Electronics e2 = setRam(16); // 16 GB RAM

        int result = comparer.compareElectronics(e1, e2, true);

        assertTrue("Comparison with r=true should favor lower RAM of e1", result < 0);
    }

    /**
     * Test the comparison when r is false (reverse order based on RAM).
     */
    @Test
    public void testCompareElectronicsWithRFalse() {
        RamComparer comparer = new RamComparer();
        Electronics e1 = setRam(8);  // 8 GB RAM
        Electronics e2 = setRam(16); // 16 GB RAM

        int result = comparer.compareElectronics(e1, e2, false);

        assertTrue("Comparison with r=false should favor higher RAM of e2", result > 0);
    }
}
