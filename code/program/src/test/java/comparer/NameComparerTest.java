package comparer;

import electronic_store.*;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for the NameComparer class.
 */
public class NameComparerTest {

    /**
     * method generate a sample computer
     * @param name
     * @return Computer
     */
    private Computer setName(String name) {
        return new Computer(name, "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
    }

    /**
     * Test the comparison when r is true (natural order).
     */
    @Test
    public void testCompareElectronicsWithRTrue() {
        NameComparer comparer = new NameComparer();
        Electronics e1 = setName("Alpha");
        Electronics e2 = setName("Beta");

        int result = comparer.compareElectronics(e1, e2, true);

        assertTrue("Comparison with r=true should favor e1", result < 0);
    }

    /**
     * Test the comparison when r is false (reverse order).
     */
    @Test
    public void testCompareElectronicsWithRFalse() {
        NameComparer comparer = new NameComparer();
        Electronics e1 = setName("Alpha");
        Electronics e2 = setName("Beta");

        int result = comparer.compareElectronics(e1, e2, false);

        assertTrue("Comparison with r=false should favor e2", result > 0);
    }
}
