package comparer;

import electronic_store.*;
import function.*;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for the StorageComparer class.
 */
public class StorageComparerTest {
    /**
     * Method to generate a sample computer
     * @param storage
     * @return Computer
     */
    private Computer setStorage(double storage) {
        return new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", storage, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
    }
    
    /**
     * Test the comparison when r is true (natural order based on storage).
     */
    @Test
    public void testCompareElectronicsWithRTrue() {
        StorageComparer comparer = new StorageComparer();
        Electronics e1 = setStorage(256);  // 256 GB storage
        Electronics e2 = setStorage(512);  // 512 GB storage

        int result = comparer.compareElectronics(e1, e2, true);

        assertTrue("Comparison with r=true should favor lower storage of e1", result < 0);
    }

    /**
     * Test the comparison when r is false (reverse order based on storage).
     */
    @Test
    public void testCompareElectronicsWithRFalse() {
        StorageComparer comparer = new StorageComparer();
        Electronics e1 = setStorage(256);  // 256 GB storage
        Electronics e2 = setStorage(512);  // 512 GB storage

        int result = comparer.compareElectronics(e1, e2, false);

        assertTrue("Comparison with r=false should favor higher storage of e2", result > 0);
    }
}
