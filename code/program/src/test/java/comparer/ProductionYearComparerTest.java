package comparer;

import electronic_store.*;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for the ProductionYearComparer class.
 */
public class ProductionYearComparerTest {

    /**
     * Method generate a sample computer
     * @param py
     * @return Computer
     */
    private Computer setProductionYear(int py) {
        return new Computer("Model X", "ID123", "BrandY", "Laptop",
            py, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
    }

    /**
     * Test the comparison when r is true (natural order based on production year).
     */
    @Test
    public void testCompareElectronicsWithRTrue() {
        ProductionYearComparer comparer = new ProductionYearComparer();
        Electronics e1 = setProductionYear(2010);
        Electronics e2 = setProductionYear(2020);

        int result = comparer.compareElectronics(e1, e2, true);

        assertTrue("Comparison with r=true should favor earlier production year of e1", result < 0);
    }

    /**
     * Test the comparison when r is false (reverse order based on production year).
     */
    @Test
    public void testCompareElectronicsWithRFalse() {
        ProductionYearComparer comparer = new ProductionYearComparer();
        Electronics e1 = setProductionYear(2010);
        Electronics e2 = setProductionYear(2020);

        int result = comparer.compareElectronics(e1, e2, false);

        assertTrue("Comparison with r=false should favor later production year of e2", result > 0);
    }
}
