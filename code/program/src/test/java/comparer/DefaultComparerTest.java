package comparer;

import electronic_store.*;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for the DefaultComparer class.
 */
public class DefaultComparerTest {
    
    /**
     * Test compareTo two for computer objects of Production Year
     * Expect the result to be the comparison of c1 to c2.
     */
    @Test
    public void testDefaultComparerWithProductionYear() {
        DefaultComparer comparer = new DefaultComparer(); 
        Computer c1 = new Computer("Model X", "ID123", "BrandY", "Laptop", 2022, 2, 1500.00,
            "High-end gaming laptop", 512, 16, 10, "Intel Core i7",
            "Windows 10", "NVIDIA GTX 1080", true);
        Computer c2 = new Computer("Model X", "ID123", "BrandY", "Laptop", 2023, 2, 1500.00,
            "High-end gaming laptop", 512, 16, 10, "Intel Core i7",
            "Windows 10", "NVIDIA GTX 1080", true);
        assertTrue(comparer.compareElectronics(c1, c2, true) < 0); 
    }

    /**
     * Test compareTo two for computer objects of warranty
     * Expect the result to be the comparison of c1 to c2.
     */
    @Test
    public void testDefaultComparerWithWarranty() {
        DefaultComparer comparer = new DefaultComparer();
        Computer c1 = new Computer("Model X", "ID123", "BrandY", "Laptop", 2023, 1, 1500.00,
            "High-end gaming laptop", 512, 16, 10, "Intel Core i7",
            "Windows 10", "NVIDIA GTX 1080", true);
        Computer c2 = new Computer("Model X", "ID124", "BrandY", "Laptop", 2023, 2, 1500.00,
            "High-end gaming laptop", 512, 16, 10, "Intel Core i7",
            "Windows 10", "NVIDIA GTX 1080", true);
        assertTrue(comparer.compareElectronics(c1, c2, true) < 0); 
    }

    /**
     * Test compareTo two for computer objects of price
     * Expect the result to be the comparison of c1 to c2.
     */
    @Test
    public void testDefaultComparerWithPrice() {
        DefaultComparer comparer = new DefaultComparer();
        Computer c1 = new Computer("Model X", "ID123", "BrandY", "Laptop", 2023, 2, 1000.00,
            "High-end gaming laptop", 512, 16, 10, "Intel Core i7",
            "Windows 10", "NVIDIA GTX 1080", true);
        Computer c2 = new Computer("Model X", "ID124", "BrandY", "Laptop", 2023, 2, 1500.00,
            "High-end gaming laptop", 512, 16, 10, "Intel Core i7",
            "Windows 10", "NVIDIA GTX 1080", true);
        assertTrue(comparer.compareElectronics(c1, c2, true) < 0); 
    }

    /**
     * Test compareTo two for computer objects of storage
     * Expect the result to be the comparison of c1 to c2.
     */
    @Test
    public void testDefaultComparerWithStorage() {
        DefaultComparer comparer = new DefaultComparer();
        Computer c1 = new Computer("Model X", "ID123", "BrandY", "Laptop", 2023, 2, 1500.00,
            "High-end gaming laptop", 500, 16, 10, "Intel Core i7",
            "Windows 10", "NVIDIA GTX 1080", true);
        Computer c2 = new Computer("Model X", "ID124", "BrandY", "Laptop", 2023, 2, 1500.00,
            "High-end gaming laptop", 512, 16, 10, "Intel Core i7",
            "Windows 10", "NVIDIA GTX 1080", true);
        assertTrue(comparer.compareElectronics(c1, c2, true) < 0); 
    }

    /**
     * Test compareTo two for computer objects of ram
     * Expect the result to be the comparison of c1 to c2.
     */
    @Test
    public void testDefaultComparerWithRam() {
        DefaultComparer comparer = new DefaultComparer();
        Computer c1 = new Computer("Model X", "ID123", "BrandY", "Laptop", 2023, 2, 1500.00,
            "High-end gaming laptop", 512, 8, 10, "Intel Core i7",
            "Windows 10", "NVIDIA GTX 1080", true);
        Computer c2 = new Computer("Model X", "ID124", "BrandY", "Laptop", 2023, 2, 1500.00,
            "High-end gaming laptop", 512, 16, 10, "Intel Core i7",
            "Windows 10", "NVIDIA GTX 1080", true);
        assertTrue(comparer.compareElectronics(c1, c2, true) < 0); 
    }

    /**
     * Test compareTo two for computer objects of quantity
     * Expect the result to be the comparison of c1 to c2.
     */
    @Test
    public void testDefaultComparerWithQuantity() {
        DefaultComparer comparer = new DefaultComparer();
        Computer c1 = new Computer("Model X", "ID123", "BrandY", "Laptop", 2023, 2, 1500.00,
            "High-end gaming laptop", 512, 16, 8, "Intel Core i7",
            "Windows 10", "NVIDIA GTX 1080", true);
        Computer c2 = new Computer("Model X", "ID124", "BrandY", "Laptop", 2023, 2, 1500.00,
            "High-end gaming laptop", 512, 16, 10, "Intel Core i7",
            "Windows 10", "NVIDIA GTX 1080", true);
        assertTrue(comparer.compareElectronics(c1, c2, true) < 0); 
    }
}
