package electronic_store;

import org.junit.Test;
import static org.junit.Assert.*;

public class ElectronicsTest {

    // Test for the constructor
    public void testConstructor() {
        Electronics electronics = new Computer("Laptop", "ID123", "BrandX", "Computers",
            2020, 2, 1500.00, "High-end gaming laptop", 512, 16, 10, "CPU", "Windows", "RTX3060", true);
        
        assertEquals("Laptop", electronics.getName());
        assertEquals("ID123", electronics.getID());
        assertEquals("BrandX", electronics.getBrand());
        assertEquals("Computers", electronics.getCategory());
        assertEquals(2020, electronics.getProductionYear());
        assertEquals(2, electronics.getWarranty());
        assertEquals(1500.00, electronics.getPrice(), 0.0);
        assertEquals("High-end gaming laptop", electronics.getDescription());
        assertEquals(512.0, electronics.getStorage(), 0.0);
        assertEquals(16.0, electronics.getRam(), 0.0);
        assertEquals(10, electronics.getQuantity());
    }

    // Tests for getters
    @Test
    public void testGetName() {
        Electronics electronics = new Computer("Laptop", "ID123", "BrandX", "Computers",
            2020, 2, 1500.00, "High-end gaming laptop", 512, 16, 10, "CPU", "Windows", "RTX3060", true);
        assertEquals("Laptop", electronics.getName());
    }

    @Test
    public void testGetID() {
        Electronics electronics = new Computer("Laptop", "ID123", "BrandX", "Computers",
            2020, 2, 1500.00, "High-end gaming laptop", 512, 16, 10, "CPU", "Windows", "RTX3060", true);
        assertEquals("ID123", electronics.getID());
    }

    @Test
    public void testGetBrand() {
        Electronics electronics = new Computer("Laptop", "ID123", "BrandX", "Computers",
            2020, 2, 1500.00, "High-end gaming laptop", 512, 16, 10, "CPU", "Windows", "RTX3060", true);
        assertEquals("BrandX", electronics.getBrand());
    }

    @Test
    public void testGetCategory() {
        Electronics electronics = new Computer("Laptop", "ID123", "BrandX", "Computers",
            2020, 2, 1500.00, "High-end gaming laptop", 512, 16, 10, "CPU", "Windows", "RTX3060", true);
        assertEquals("Computers", electronics.getCategory());
    }

    @Test
    public void testGetProductionYear() {
        Electronics electronics = new Computer("Laptop", "ID123", "BrandX", "Computers",
            2020, 2, 1500.00, "High-end gaming laptop", 512, 16, 10, "CPU", "Windows", "RTX3060", true);
        assertEquals(2020, electronics.getProductionYear());
    }

    @Test
    public void testGetWarranty() {
        Electronics electronics = new Computer("Laptop", "ID123", "BrandX", "Computers",
            2020, 2, 1500.00, "High-end gaming laptop", 512, 16, 10, "CPU", "Windows", "RTX3060", true);
        assertEquals(2, electronics.getWarranty());
    }

    @Test
    public void testGetPrice() {
        Electronics electronics = new Computer("Laptop", "ID123", "BrandX", "Computers",
            2020, 2, 1500.00, "High-end gaming laptop", 512, 16, 10, "CPU", "Windows", "RTX3060", true);
        assertEquals(1500.00, electronics.getPrice(), 0.0);
    }

    @Test
    public void testGetDescription() {
        Electronics electronics = new Computer("Laptop", "ID123", "BrandX", "Computers",
            2020, 2, 1500.00, "High-end gaming laptop", 512, 16, 10, "CPU", "Windows", "RTX3060", true);
        assertEquals("High-end gaming laptop", electronics.getDescription());
    }

    @Test
    public void testGetStorage() {
        Electronics electronics = new Computer("Laptop", "ID123", "BrandX", "Computers",
            2020, 2, 1500.00, "High-end gaming laptop", 512, 16, 10, "CPU", "Windows", "RTX3060", true);
        assertEquals(512.0, electronics.getStorage(), 0.0);
    }

    @Test
    public void testGetRam() {
        Electronics electronics = new Computer("Laptop", "ID123", "BrandX", "Computers",
            2020, 2, 1500.00, "High-end gaming laptop", 512, 16, 10, "CPU", "Windows", "RTX3060", true);
        assertEquals(16.0, electronics.getRam(), 0.0);
    }

    @Test
    public void testGetQuantity() {
        Electronics electronics = new Computer("Laptop", "ID123", "BrandX", "Computers",
            2020, 2, 1500.00, "High-end gaming laptop", 512, 16, 10, "CPU", "Windows", "RTX3060", true);
        assertEquals(10, electronics.getQuantity());
    }

    // Tests for setters
    @Test
    public void testSetName() {
        Electronics electronics = createDefaultElectronics();
        electronics.setName("Desktop");
        assertEquals("Desktop", electronics.getName());
    }

    @Test
    public void testSetID() {
        Electronics electronics = createDefaultElectronics();
        electronics.setID("ID456");
        assertEquals("ID456", electronics.getID());
    }

    @Test
    public void testSetBrand() {
        Electronics electronics = createDefaultElectronics();
        electronics.setBrand("BrandY");
        assertEquals("BrandY", electronics.getBrand());
    }

    @Test
    public void testSetCategory() {
        Electronics electronics = createDefaultElectronics();
        electronics.setCategory("Electronics");
        assertEquals("Electronics", electronics.getCategory());
    }

    @Test
    public void testSetProductionYear() {
        Electronics electronics = createDefaultElectronics();
        electronics.setProductionYear(2021);
        assertEquals(2021, electronics.getProductionYear());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetProductionYearInvalid() {
        Electronics electronics = createDefaultElectronics();
        electronics.setProductionYear(1700);
    }

    @Test
    public void testSetWarranty() {
        Electronics electronics = createDefaultElectronics();
        electronics.setWarranty(3);
        assertEquals(3, electronics.getWarranty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetWarrantyInvalid() {
        Electronics electronics = createDefaultElectronics();
        electronics.setWarranty(-1);
    }

    @Test
    public void testSetPrice() {
        Electronics electronics = createDefaultElectronics();
        electronics.setPrice(2000.00);
        assertEquals(2000.00, electronics.getPrice(), 0.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetPriceInvalid() {
        Electronics electronics = createDefaultElectronics();
        electronics.setPrice(-1000.00);
    }

    @Test
    public void testSetDescription() {
        Electronics electronics = createDefaultElectronics();
        electronics.setDescription("Budget laptop");
        assertEquals("Budget laptop", electronics.getDescription());
    }

    @Test
    public void testSetStorage() {
        Electronics electronics = createDefaultElectronics();
        electronics.setStorage(1024);
        assertEquals(1024.0, electronics.getStorage(), 0.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetStorageInvalid() {
        Electronics electronics = createDefaultElectronics();
        electronics.setStorage(-100.0);
    }

    @Test
    public void testSetRam() {
        Electronics electronics = createDefaultElectronics();
        electronics.setRam(32);
        assertEquals(32.0, electronics.getRam(), 0.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetRamInvalid() {
        Electronics electronics = createDefaultElectronics();
        electronics.setRam(-8.0);
    }

    @Test
    public void testSetQuantity() {
        Electronics electronics = createDefaultElectronics();
        electronics.setQuantity(20);
        assertEquals(20, electronics.getQuantity());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetQuantityInvalid() {
        Electronics electronics = createDefaultElectronics();
        electronics.setQuantity(-5);
    }

    @Test
    public void testAddRam() {
        Electronics electronics = createDefaultElectronics();
        electronics.addRam(4);
        assertEquals(20, electronics.getRam(), 0.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddRamInvalid() {
        Electronics electronics = createDefaultElectronics();
        electronics.addRam(-20);
    }

    // Test for addStorage method
    @Test
    public void testAddStorage() {
        Electronics electronics = createDefaultElectronics();
        electronics.addStorage(128);
        assertEquals(640, electronics.getStorage(), 0.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddStorageInvalid() {
        Electronics electronics = createDefaultElectronics();
        electronics.addStorage(-600);
    }

    // Test for verifyQuantity method
    @Test
    public void testVerifyQuantityTrue() {
        Electronics electronics = createDefaultElectronics();
        assertFalse(electronics.verifyQuantity(5));
    }

    @Test
    public void testVerifyQuantityFalse() {
        Electronics electronics = createDefaultElectronics();
        assertTrue(electronics.verifyQuantity(15));
    }

    // Helper method to create a default Electronics object
    private Electronics createDefaultElectronics() {
        return new Computer("Laptop", "ID123", "BrandX", "Computers",
            2020, 2, 1500.00, "High-end gaming laptop", 512, 16, 10, "CPU", "Windows", "RTX3060", true);
    }

    

       
    }

