package electronic_store;

import org.junit.Test;
import static org.junit.Assert.*;

public class ServerTest {

    @Test
public void testConstructor() {
    Server server = new Server("ServerName", "ID001", "BrandX", "Servers", 2020, 3, 5000.0, "Server Description", 1024, 64, 5, "Intel Xeon", "SSD", "NVIDIA Tesla", "Ethernet", 1000, "RAID 5");
    assertEquals("ServerName", server.getName());
    assertEquals("ID001", server.getID());
    assertEquals("BrandX", server.getBrand());
    assertEquals("Servers", server.getCategory());
    assertEquals(2020, server.getProductionYear());
    assertEquals(3, server.getWarranty());
    assertEquals(5000.0, server.getPrice(), 0.0);
    assertEquals("Server Description", server.getDescription());
    assertEquals(1024.0, server.getStorage(), 0.0);
    assertEquals(64.0, server.getRam(), 0.0);
    assertEquals(5, server.getQuantity());
    assertEquals("Intel Xeon", server.getSCPU());
    assertEquals("SSD", server.getStorageSystem());
    assertEquals("NVIDIA Tesla", server.getCalculationGPU());
    assertEquals("Ethernet", server.getPort());
    assertEquals(1000.0, server.getBandwidth(), 0.0);
    assertEquals("RAID 5", server.getRaid());
}

    // Tests for getters
    @Test
    public void testGetSCPU() {
        Server server = createDefaultServer();
        assertEquals("Intel Xeon", server.getSCPU());
    }

    @Test
    public void testGetStorageSystem() {
        Server server = createDefaultServer();
        assertEquals("SSD", server.getStorageSystem());
    }

    @Test
    public void testGetCalculationGPU() {
        Server server = createDefaultServer();
        assertEquals("NVIDIA Tesla", server.getCalculationGPU());
    }

    @Test
    public void testGetPort() {
        Server server = createDefaultServer();
        assertEquals("Ethernet", server.getPort());
    }

    @Test
    public void testGetBandwidth() {
        Server server = createDefaultServer();
        assertEquals(1000.0, server.getBandwidth(), 0.0);
    }

    @Test
    public void testGetRaid() {
        Server server = createDefaultServer();
        assertEquals("RAID 5", server.getRaid());
    }

    // Tests for setters
    @Test
    public void testSetSCPU() {
        Server server = createDefaultServer();
        server.setSCPU("AMD Ryzen");
        assertEquals("AMD Ryzen", server.getSCPU());
    }

    @Test
    public void testSetStorageSystem() {
        Server server = createDefaultServer();
        server.setStorageSystem("HDD");
        assertEquals("HDD", server.getStorageSystem());
    }

    @Test
    public void testSetCalculationGPU() {
        Server server = createDefaultServer();
        server.setCalculationGPU("AMD Radeon");
        assertEquals("AMD Radeon", server.getCalculationGPU());
    }

    @Test
    public void testSetPort() {
        Server server = createDefaultServer();
        server.setPort("WiFi");
        assertEquals("WiFi", server.getPort());
    }

    @Test
    public void testSetBandwidth() {
        Server server = createDefaultServer();
        server.setBandwidth(500.0);
        assertEquals(500.0, server.getBandwidth(), 0.0);
    }

    @Test
    public void testSetRaid() {
        Server server = createDefaultServer();
        server.setRaid("RAID 1");
        assertEquals("RAID 1", server.getRaid());
    }

    // Test for toString method
    @Test
    public void testToString() {
        Server server = createDefaultServer();
        String expectedString = "Name: ServerName, ID: ID001, Brand: BrandX, Category: Servers, Year of production: 2020, Warranty(years): 3, Price 5000.0, Description: Server Description, Storage(Gb): 1024.0, Ram(Gb): 64.0, SCPU='Intel Xeon', Storage System: SSD, Calculation GPU: NVIDIA Tesla, Port: Ethernet, Bandwidth: 1000.0, Raid: RAID 5";
        assertEquals(expectedString, server.toString());
    }
    // Test for compareTo method
    @Test
    public void testCompareTo() {
        Server server1 = createDefaultServer();
        Server server2 = createDefaultServer();
        assertEquals(0, server1.compareTo(server2));
    }
    private Server createDefaultServer() {
        return new Server("ServerName", "ID001", "BrandX", "Servers", 2020, 3, 5000.0, "Server Description", 1024, 64, 5, "Intel Xeon", "SSD", "NVIDIA Tesla", "Ethernet", 1000, "RAID 5");
    }
}
