package electronic_store;

import org.junit.Test;
import static org.junit.Assert.*;

public class PromotionTest {

    @Test
    public void testGetPromotionMultiplier() {
        Promotion promotion = new Promotion();
        assertEquals(1.0, promotion.getPromotionMultiplier(), 0.0);
    }

    @Test
    public void testApplicablePromotionForStudent() {
        Promotion promotion = new Promotion();
        Customer studentCustomer = new Customer("Name", "C0002S", 15, "sample.email", "sample address");
        promotion.testApplicablePromotion(studentCustomer);
        assertEquals(0.8, promotion.getPromotionMultiplier(), 0.0);
    }

    @Test
    public void testApplicablePromotionForNonAdult() {
        Promotion promotion = new Promotion();
        Customer nonAdultCustomer = setAge(17);
        promotion.testApplicablePromotion(nonAdultCustomer);
        assertEquals(0.9, promotion.getPromotionMultiplier(), 0.0);
    }

    @Test
    public void testApplicablePromotionForElderly() {
        Promotion promotion = new Promotion();
        Customer elderlyCustomer = setAge(65);
        promotion.testApplicablePromotion(elderlyCustomer);
        assertEquals(0.75, promotion.getPromotionMultiplier(), 0.0);
    }

    @Test
    public void testApplicablePromotionForRegularCustomer() {
        Promotion promotion = new Promotion();
        Customer regularCustomer = setAge(40);
        promotion.testApplicablePromotion(regularCustomer);
        assertEquals(1.0, promotion.getPromotionMultiplier(), 0.0);
    }

    private Customer setAge(int age) {
        return new Customer("C1", "C0001", age, "sample@gamil.com", "Montreal");
    }
}
