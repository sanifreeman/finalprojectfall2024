package electronic_store;

import org.junit.Test;
import static org.junit.Assert.*;

public class OrderTest {

    // Test for the constructor
    @Test
    public void testConstructor() {
        Order order = new Order("ORD123", "CUST456", "PROD789", 5, 200.0);
        assertEquals("ORD123", order.getOrderID());
        assertEquals("CUST456", order.getCustomerID());
        assertEquals("PROD789", order.getProductID());
        assertEquals(5, order.getQuantity());
        assertEquals(200.0, order.getPrice(), 0.0);
    }

    // Tests for getters
    @Test
    public void testGetOrderID() {
        Order order = createDefaultOrder();
        assertEquals("ORD123", order.getOrderID());
    }

    @Test
    public void testGetCustomerID() {
        Order order = createDefaultOrder();
        assertEquals("CUST456", order.getCustomerID());
    }

    @Test
    public void testGetProductID() {
        Order order = createDefaultOrder();
        assertEquals("PROD789", order.getProductID());
    }

    @Test
    public void testGetQuantity() {
        Order order = createDefaultOrder();
        assertEquals(5, order.getQuantity());
    }

    @Test
    public void testGetPrice() {
        Order order = createDefaultOrder();
        assertEquals(200.0, order.getPrice(), 0.0);
    }

    @Test
    public void testGetTypeName() {
        Order order = createDefaultOrder();
        assertEquals("Order_typ", order.getTypeName());
    }

    // Tests for setters
    @Test
    public void testSetOrderID() {
        Order order = createDefaultOrder();
        order.setOrderID("ORD456");
        assertEquals("ORD456", order.getOrderID());
    }

    @Test
    public void testSetCustomerID() {
        Order order = createDefaultOrder();
        order.setCustomerID("CUST789");
        assertEquals("CUST789", order.getCustomerID());
    }

    @Test
    public void testSetProductID() {
        Order order = createDefaultOrder();
        order.setProductID("PROD321");
        assertEquals("PROD321", order.getProductID());
    }

    @Test
    public void testSetQuantity() {
        Order order = createDefaultOrder();
        order.setQuantity(10);
        assertEquals(10, order.getQuantity());
    }

    @Test
    public void testSetPrice() {
        Order order = createDefaultOrder();
        order.setPrice(300.0);
        assertEquals(300.0, order.getPrice(), 0.0);
    }

    @Test
    public void testSetTypeName() {
        Order order = createDefaultOrder();
        order.setTypeName("NewOrderType");
        assertEquals("NewOrderType", order.getTypeName());
    }

    // Test for toString method
    @Test
    public void testToString() {
        Order order = createDefaultOrder();
        String expectedString = " orderID: ORD123, customerID: CUST456, productID: PROD789, quantity: 5, price: 200.0, typeName: Order_typ";
        assertEquals(expectedString, order.toString());
    }

    private Order createDefaultOrder() {
        return new Order("ORD123", "CUST456", "PROD789", 5, 200.0);
    }
}
