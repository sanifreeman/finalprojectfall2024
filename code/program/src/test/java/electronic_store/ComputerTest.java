package electronic_store;

import org.junit.Test;
import static org.junit.Assert.*;

public class ComputerTest {

    // Test for the constructor
    @Test
    public void testConstructor() {
        Computer computer = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
    
        assertEquals("Model X", computer.getName());
        assertEquals("ID123", computer.getID());
        assertEquals("BrandY", computer.getBrand());
        assertEquals("Laptop", computer.getCategory());
        assertEquals(2023, computer.getProductionYear());
        assertEquals(2, computer.getWarranty());
        assertEquals(1500.00, computer.getPrice(), 0.0);
        assertEquals("High-end gaming laptop", computer.getDescription());
        assertEquals(512, computer.getStorage(), 0.0);
        assertEquals(16, computer.getRam(), 0.0);
        assertEquals(10, computer.getQuantity());
        assertEquals("Intel Core i7", computer.getCPU());
        assertEquals("Windows 10", computer.getSystem());
        assertEquals("NVIDIA GTX 1080", computer.getGraphicCard());
        assertTrue(computer.isPortable());
    }

    // Test for the copy constructor
    @Test
    public void testCopyConstructor() {
        Computer original = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
        Computer copy = new Computer(original);
        assertEquals(original.getCPU(), copy.getCPU());
    }

    // Tests for getters
    @Test
    public void testGetCPU() {
        Computer computer = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
        assertEquals("Intel Core i7", computer.getCPU());
    }

    @Test
    public void testGetSystem() {
        Computer computer = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
        assertEquals("Windows 10", computer.getSystem());
    }

    @Test
    public void testGetGraphicCard() {
        Computer computer = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
        assertEquals("NVIDIA GTX 1080", computer.getGraphicCard());
    }

    @Test
    public void testIsPortable() {
        Computer computer = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
        assertTrue(computer.isPortable());
    }

    @Test
    public void testGetTypeName() {
        Computer computer = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
        assertEquals("Computer_typ", computer.getTypeName());
    }

    // Tests for setters
    @Test
    public void testSetCPU() {
        Computer computer = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
        computer.setCPU("AMD Ryzen 5");
        assertEquals("AMD Ryzen 5", computer.getCPU());
    }

    @Test
    public void testSetSystem() {
        Computer computer = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
        computer.setSystem("Linux");
        assertEquals("Linux", computer.getSystem());
    }

    @Test
    public void testSetGraphicCard() {
        Computer computer = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
        computer.setGraphicCard("AMD Radeon RX 6800");
        assertEquals("AMD Radeon RX 6800", computer.getGraphicCard());
    }

    @Test
    public void testSetPortable() {
        Computer computer = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
        computer.setPortable(false);
        assertFalse(computer.isPortable());
    }

    @Test
    public void testSetTypeName() {
        Computer computer = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);
        computer.setTypeName("NewComputerType");
        assertEquals("NewComputerType", computer.getTypeName());
    }

    // Tests for compareTo method
    @Test
    public void testCompareToEqual() {
        Computer computer1 = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);

        Computer computer2 = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);

        assertEquals(0, computer1.compareTo(computer2));
    }

    @Test
    public void testCompareToNotEqual() {
        Computer computer1 = new Computer("Model X", "ID123", "BrandY", "Laptop",
            2023, 2, 1500.00, "High-end gaming laptop", 512, 16,
            10, "Intel Core i7", "Windows 10", "NVIDIA GTX 1080", true);

        Computer computer2 = new Computer("Model Y", "ID124", "BrandZ", "Desktop",
            2022, 1, 1000.00, "Office desktop", 256, 8,
            5, "Intel Core i5", "Windows 11", "NVIDIA GTX 1060", false);

        assertTrue(computer1.compareTo(computer2) != 0);
    }
}
