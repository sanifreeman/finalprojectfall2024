package query;
import electronic_store.Electronics;
import function.Storage;
import java.util.*;
/**
 * Implements a RAM-based query on electronic products.
 * @author Hasani
 */
public class RamQuery implements Query{
    /**
     * Queries electronic products based on their RAM size.
     * Returns a list of products where each product's RAM is less than the specified value.
     *
     * @param s The storage system containing the electronic products.
     * @param str The RAM size (as a string) to be used as the query criterion.
     * @return A list of Electronics that meet the query criterion.
     */
    public List<Electronics> queryProducts(Storage s, String str) {
        double ram = Double.parseDouble(str);
        List<Electronics> queryList = new ArrayList<Electronics>();
        for(Electronics e : s.getElectronicsList()) {
            if(e.getRam() < ram) {
                queryList.add(e);
            }
        }
        return queryList;
    }
}
