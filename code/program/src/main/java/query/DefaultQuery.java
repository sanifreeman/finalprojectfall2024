package query;
import electronic_store.*;
import function.*;
import java.util.*;
/**
 * Implements a default query for electronic products.
 * This class provides a basic query mechanism that returns a sorted list of electronic products based on a default criterion.
 */
public class DefaultQuery implements Query{
    /**
     * Queries electronic products using a default sorting criterion.
     *
     * @param s The storage system containing the electronic products.
     * @param str An optional string parameter, not used in this default implementation.
     * @return A sorted list of Electronics as per the default sorting criterion.
     */
    public List<Electronics> queryProducts(Storage s, String str) {
        Sorting sortObject = new Sorting(TypeEnum.DEFAULT, false, s.getElectronicsList());
        return new ArrayList<>(sortObject.getList());
    }
}
