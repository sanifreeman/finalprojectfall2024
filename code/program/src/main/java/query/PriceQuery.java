package query;
import electronic_store.Electronics;
import function.Storage;
import java.util.*;
/**
 * Implements a query based on the price of electronic products.
 * This class filters and retrieves electronic products whose price is less than a specified value.
 */
public class PriceQuery implements Query{
    /**
     * Queries electronic products based on their price.
     *
     * @param s The storage system containing the electronic products.
     * @param str The price (as a string) to be used as the query criterion.
     * @return A list of Electronics that meet the query criterion.
     */
    public List<Electronics> queryProducts(Storage s, String str) {
        double price = Double.parseDouble(str);
        List<Electronics> queryList = new ArrayList<Electronics>();
        for(Electronics e : s.getElectronicsList()) {
            if(e.getPrice() < price) {
                queryList.add(e);
            }
        }
        return queryList;
    }
}
