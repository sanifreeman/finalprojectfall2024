package query;
import electronic_store.*;
import function.Storage;

import java.util.*;
/**
 * Interface defining the structure for querying electronic products.
 * Implementations of this interface should provide the logic for querying electronic products based on specific criteria.
 * @author Tanrui
 */

public interface Query {
    /**
     * Queries electronic products based on a specified criterion.
     *
     * @param s The storage system containing the electronic products.
     * @param str The string representing the query criterion.
     * @return A list of Electronics that meet the query criterion.
     */
    public List<Electronics> queryProducts(Storage s, String str);
}
