package query;
import electronic_store.Electronics;
import function.Storage;
import java.util.*;

public class StorageQuery implements Query{
    public List<Electronics> queryProducts(Storage s, String str) {
        double storage = Double.parseDouble(str);
        List<Electronics> queryList = new ArrayList<Electronics>();
        for(Electronics e : s.getElectronicsList()) {
            if(e.getStorage() < storage) {
                queryList.add(e);
            }
        }
        return queryList;
    }
}
