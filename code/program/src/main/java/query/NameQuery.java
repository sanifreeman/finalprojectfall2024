package query;
import electronic_store.Electronics;
import function.Storage;
import java.util.*;
/**
 * Implements a query based on the name of electronic products.
@author Hasani 
 */

public class NameQuery implements Query{
    /**
     * Queries electronic products based on their name.
   
     * @param s The storage system containing the electronic products.
     * @param name The name to be used as the query criterion.
     * @return A list of Electronics that meet the query criterion.
     */
    public List<Electronics> queryProducts(Storage s, String name) {
        List<Electronics> queryList = new ArrayList<Electronics>();
        for(Electronics e : s.getElectronicsList()) {
            if(e.getName().equalsIgnoreCase(name)) {
                queryList.add(e);
            }
        }
        return queryList;
    }
}
