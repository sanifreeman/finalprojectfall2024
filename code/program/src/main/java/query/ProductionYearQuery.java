package query;
import electronic_store.Electronics;
import function.Storage;
import java.util.*;
/**
 * Implements a query based on the production year of electronic products.
 * This class filters and retrieves electronic products whose production year is less than a specified value.
 * @author Tianrui
 */
public class ProductionYearQuery implements Query{
    /**
     * Queries electronic products based on their production year.
     *
     * @param s The storage system containing the electronic products.
     * @param str The production year (as a string) to be used as the query criterion.
     * @return A list of Electronics that meet the query criterion.
     */
    public List<Electronics> queryProducts(Storage s, String str) {
        int productionYear = Integer.parseInt(str);
        List<Electronics> queryList = new ArrayList<Electronics>();
        for(Electronics e : s.getElectronicsList()) {
            if(e.getProductionYear() < productionYear) {
                queryList.add(e);
            }
        }
        return queryList;
    }
}
