package function;
import query.*;
import java.util.*;

import javax.management.Query;

import electronic_store.*;
/**
 * Handles querying and displaying electronic products based on specific criteria.
 * This class implements the Display interface to show electronic products as per the specified query criteria.
 * @author Tianrui
 */
public class Querying implements Display{
    private Storage storage;
    private Query q;
    private List<Electronics> eList;
    private String queryStr;
    /**
     * Constructor for creating a Querying instance.
     * @param storage The storage object containing the electronics data.
     * @param type The type of query to be performed.
     * @param queryStr The string based on which the query will be executed.
     */
    public Querying(Storage storage, TypeEnum type, String queryStr) {
        this.storage = storage;
        assignQuery(type);
        this.eList = new ArrayList<>(storage.getElectronicsList());
        this.queryStr = queryStr;
    }
    /**
     * Displays products based on the specified query.
     */
    public void displayProducts() {
        eList = new ArrayList<Electronics>(q.queryProducts(storage, queryStr));
        for(Electronics e : eList) {
            System.out.println(e);
        }
    }
    /**
     * Assigns a specific query type based on the provided TypeEnum.
     * @param type The type of query to be assigned.
     */
    private void assignQuery(TypeEnum type) {
        if(type.equals(TypeEnum.NAME)) {
            this.q = new NameQuery();
        }else if(type.equals(TypeEnum.PRICE)) {
            this.q = new PriceQuery();
        }else if(type.equals(TypeEnum.PRODUCTIONYEAR)) {
            this.q = new ProductionYearQuery();
        }else if(type.equals(TypeEnum.RAM)) {
            this.q = new RamQuery();
        }else if(type.equals(TypeEnum.STORAGE)) {
            this.q = new StorageQuery();
        }else {
            this.q = new DefaultQuery();
        }
    }
    /**
     * Retrieves the list of electronics that match the query.
     * @return A list of Electronics objects that satisfy the query criteria.
     */
    public List<Electronics> getList() {
        return this.eList;
    }
}
