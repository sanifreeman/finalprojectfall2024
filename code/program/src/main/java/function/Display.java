package function;
/**
 * Interface for displaying product information in an electronic store management system.
 
 * @author Tianrui
 */
public interface Display {
    public abstract void displayProducts();
}
