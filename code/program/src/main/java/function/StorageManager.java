package function;
import java.io.IOException;
import electronic_store.*;
import cexception.*;

public class StorageManager {
    private Storage storage;

    public StorageManager(Storage storage) {
        this.storage = storage;
    }

    public void addComputer(Computer c) {
        this.storage.electronicList.add(c);
    }

    public void addServer(Server s) {
        this.storage.electronicList.add(s);
    }

    public void addCustomer(Customer c) {
        this.storage.customerList.add(c);
    }

    public void addOrder(Order o) {
        this.storage.orderList.add(o);
    }

    /**
     * Modifies the quantity of an electronic item.
     *
     * @param ID The ID of the electronic item to modify.
     * @param newQuantity The new quantity to set for the electronic item.
     * @throws DataException 
     */
    public void modifyQuantity(String ID, int newQuantity) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID)) {
                e.setQuantity(newQuantity);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the name of an electronic item.
     *
     * @param ID The ID of the electronic item to modify.
     * @param newName The new name to set for the electronic item.
     * @throws DataException 
     */
    public void modifyName(String ID, String newName) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID)) {
                e.setName(newName);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the brand of an electronic item.
     *
     * @param ID The ID of the electronic item to modify.
     * @param newBrand The new brand to set for the electronic item.
     * @throws DataException
     */
    public void modifyBrand(String ID, String newBrand) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID)) {
                e.setBrand(newBrand);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the category of an electronic item.
     *
     * @param ID The ID of the electronic item to modify.
     * @param newCategory The new category to set for the electronic item.
     * @throws DataException
     */
    public void modifyCategory(String ID, String newCategory) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID)) {
                e.setCategory(newCategory);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the production year of an electronic item.
     *
     * @param ID The ID of the electronic item to modify.
     * @param newProductionYear The new production year to set for the electronic item.
     * @throws DataException
     */
    public void modifyProductionYear(String ID, int newProductionYear) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID)) {
                e.setProductionYear(newProductionYear);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the warranty period of an electronic item.
     *
     * @param ID The ID of the electronic item to modify.
     * @param newWarranty The new warranty period to set for the electronic item.
     * @throws DataException
     */
    public void modifyWarranty(String ID, int newWarranty) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID)) {
                e.setWarranty(newWarranty);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the price of an electronic item.
     *
     * @param ID The ID of the electronic item to modify.
     * @param newPrice The new price to set for the electronic item.
     * @throws DataException
     */
    public void modifyPrice(String ID, double newPrice) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID)) {
                e.setPrice(newPrice);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the description of an electronic item.
     *
     * @param ID The ID of the electronic item to modify.
     * @param newDescription The new description to set for the electronic item.
     * @throws DataException
     */
    public void modifyDescription(String ID, String newDescription) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID)) {
                e.setDescription(newDescription);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the storage capacity of an electronic item.
     *
     * @param ID The ID of the electronic item to modify.
     * @param newStorage The new storage capacity to set for the electronic item.
     * @throws DataException
     */
    public void modifyStorage(String ID, double newStorage) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID)) {
                e.setStorage(newStorage);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the RAM capacity of an electronic item.
     *
     * @param ID The ID of the electronic item to modify.
     * @param newRam The new RAM capacity to set for the electronic item.
     * @throws DataException
     */
    public void modifyRam(String ID, double newRam) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID)) {
                e.setRam(newRam);
                break;
            }
        }
        storage.writeProducts();
    }

    //Computer only

    /**
     * Modifies the CPU of a specified computer in the electronics list.
     * 
     * @param ID The unique identifier of the computer to be modified.
     * @param CPU The new CPU to be set for the computer.
     * @throws DataException
     */
    public void modifyCPU(String ID, String CPU) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID) && e instanceof Computer) {
                Computer c =(Computer)e;
                c.setCPU(CPU);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the operating system of a specified computer in the electronics list.
     * 
     * @param ID The unique identifier of the computer to be modified.
     * @param system The new operating system to be set for the computer.
     * @throws DataException
     */
    public void modifyCSystem(String ID, String system) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID) && e instanceof Computer) {
                Computer c =(Computer)e;
                c.setSystem(system);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the graphics card of a specified computer in the electronics list.
     * 
     * @param ID The unique identifier of the computer to be modified.
     * @param graphicCard The new graphics card to be set for the computer.
     * @throws DataException
     */
    public void modifyCGraphicCard(String ID, String graphicCard) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID) && e instanceof Computer) {
                Computer c =(Computer)e;
                c.setGraphicCard(graphicCard);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the portability status of a specified computer in the electronics list.
     * 
     * @param ID The unique identifier of the computer to be modified.
     * @param portable The new portability status(true for portable, false otherwise) to be set for the computer.
     * @throws DataException
     */
    public void modifyCPortable(String ID, boolean portable) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID) && e instanceof Computer) {
                Computer c =(Computer)e;
                c.setPortable(portable);
                break;
            }
        }
        storage.writeProducts();
    }

    //Server only

    /**
     * Modifies the SCPU of a server identified by its ID.
     *
     * @param ID The ID of the server to modify.
     * @param SCPU The new server cpu value to set.
     * @throws DataException
     */
    public void modifySSCPU(String ID, String SCPU) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID) && e instanceof Server) {
                Server s =(Server)e;
                s.setSCPU(SCPU);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the storage system of a server identified by its ID.
     *
     * @param ID The ID of the server to modify.
     * @param storageSystem The new storage system value to set.
     * @throws DataException
     */
    public void modifySStorageSystem(String ID, String storageSystem) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID) && e instanceof Server) {
                Server s =(Server) e;
                s.setStorageSystem(storageSystem);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the calculation GPU of a server identified by its ID.
     *
     * @param ID The ID of the server to modify.
     * @param calculationGPU The new calculation GPU value to set.
     * @throws DataException
     */
    public void modifySCalculationGPU(String ID, String calculationGPU) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID) && e instanceof Server) {
                Server s =(Server) e;
                s.setCalculationGPU(calculationGPU);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the port of a server identified by its ID.
     *
     * @param ID The ID of the server to modify.
     * @param port The new port value to set.
     * @throws DataException
     */
    public void modifySPort(String ID, String port) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID) && e instanceof Server) {
                Server s =(Server) e;
                s.setPort(port);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the bandwidth of a server identified by its ID.
    *
    * @param ID The ID of the server to modify.
    * @param bandwidth The new bandwidth value to set.
    * @throws DataException
    */
    public void modifySBandwidth(String ID, double bandwidth) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID) && e instanceof Server) {
                Server s =(Server) e;
                s.setBandwidth(bandwidth);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * Modifies the raid of a server identified by its ID.
     *
     * @param ID The ID of the server to modify.
     * @param raid The new raid value to set.
     * @throws DataException
     */
    public void modifySRaid(String ID, String raid) throws DataException{
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID) && e instanceof Server) {
                Server s =(Server) e;
                s.setRaid(raid);
                break;
            }
        }
        storage.writeProducts();
    }

    //modify customer

    /**
     * Modifies the name of a customer identified by their ID.
     *
     * @param ID The ID of the customer to modify.
     * @param ID The new name value to set.
     * @throws DataException
     */
    public void modifyCName(String ID, String name) throws DataException{
        for(Customer c : storage.customerList) {
            if(c.getID().equals(ID)) {
                c.setName(name);
                break;
            }
        }
        storage.writeCustomers();
    }

    /**
     * Modifies the age of a customer identified by their ID.
     *
     * @param ID The ID of the customer to modify.
     * @param age The new age value to set.
     * @throws DataException
     */
    public void modifyCAge(String ID, int age) throws DataException{
        for (Customer c : storage.customerList) {
            if (c.getID().equals(ID)) {
                c.setAge(age);
                break;
            }
        }
        storage.writeCustomers();
    }

    /**
     * Modifies the email of a customer identified by their ID.
     *
     * @param ID The ID of the customer to modify.
     * @param email The new email value to set.
     * @throws DataException
     */
    public void modifyCEmail(String ID, String email) throws DataException{
        for (Customer c : storage.customerList) {
            if (c.getID().equals(ID)) {
                c.setEmail(email);
                break;
            }
        }
        storage.writeCustomers();
    }

    /**
     * Modifies the address of a customer identified by their ID.
     *
     * @param ID The ID of the customer to modify.
     * @param address The new address value to set.
     * @throws DataException
     */
    public void modifyCAddress(String ID, String address) throws DataException{
        for (Customer c : storage.customerList) {
            if (c.getID().equals(ID)) {
                c.setAddress(address);
                break;
            }
        }
        storage.writeCustomers();
    }

    //delete methods
    /**
     * method delete product
     * @param ID
     * @throws DataException
     */
    public void deleteProduct(String ID) throws DataException {
        for(Electronics e : storage.electronicList) {
            if(e.getID().equals(ID)) {
                storage.electronicList.remove(e);
                break;
            }
        }
        storage.writeProducts();
    }

    /**
     * method delete customer
     * @param ID
     * @throws DataException
     */
    public void deleteCustomer(String ID) throws DataException {
        for(Customer c : storage.customerList) {
            if(c.getID().equals(ID)) {
                storage.customerList.remove(c);
                break;
            }
        }
        storage.writeCustomers();
    }

    /**
     * method delete order
     * @param ID
     * @throws DataException
     */
    public void deleteOrder(String ID) throws DataException {
        for(Order o : storage.orderList) {
            if(o.getOrderID().equals(ID)) {
                storage.orderList.remove(o);
                break;
            }
        }
    }
}