package function;
import comparer.*;
import electronic_store.*;
import java.util.*;
/**
 * Handles sorting and displaying electronic products based on specific criteria.
 * This class implements the Display interface and allows sorting of electronic products using various comparers.
 * @author Tianrui and Hasani
 */

public class Sorting implements Display{
    private Comparer c;
    private boolean reverse;
    private List<Electronics> eList;
    /**
     * Constructor for creating a Sorting instance.
     * @param type The type of sorting to be performed, based on TypeEnum.
     * @param reverse Indicates whether the sorting should be in reverse order.
     * @param list The list of Electronics to be sorted.
     */
    public Sorting(TypeEnum type, boolean reverse, List<Electronics> list) {
        this.reverse = reverse;
        this.eList = new ArrayList<Electronics>(list);
        assignComparer(type);
    }
        /**
     * Assigns a specific comparer based on the provided TypeEnum.
     * @param type The type of sorting comparer to be assigned.
     */
    private void assignComparer(TypeEnum type) {
        if(type.equals(TypeEnum.NAME)) {
            this.c = new NameComparer();
        }else if(type.equals(TypeEnum.PRICE)) {
            this.c = new PriceComparer();
        }else if(type.equals(TypeEnum.PRODUCTIONYEAR)) {
            this.c = new ProductionYearComparer();
        }else if(type.equals(TypeEnum.RAM)) {
            this.c = new RamComparer();
        }else if(type.equals(TypeEnum.STORAGE)) {
            this.c = new StorageComparer();
        }else {
            this.c = new DefaultComparer();
        }
    }
    /**
     * Displays sorted products.
     */
    public void displayProducts() {
        sort();
        for(Electronics e : this.eList) {
            System.out.println(e);
        }
    }
    /**
     * Sorts the list of electronics based on the comparer and order.
     */
    public void sort() {
        for(int i = 0; i < this.eList.size(); i ++) {
            int index = i;
            for(int j = i + 1; j < this.eList.size(); j ++) {
                if(c.compareElectronics(this.eList.get(j), this.eList.get(index), reverse) < 0) {
                    Collections.swap(this.eList, i, index);
                }
            }
        }
    }
    /**
     * Sets the sorting type based on a given TypeEnum.
     * @param type The TypeEnum to set the sorting comparer.
     */
    public void setSortingType(TypeEnum type) {
        assignComparer(type);
    }
    /**
     * Retrieves the sorted list of electronics.
     * @return A sorted list of Electronics objects.
     */
    public List<Electronics> getList() {
        return this.eList;
    }

}
