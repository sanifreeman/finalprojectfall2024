package function;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import cexception.DataException;
import electronic_store.*;
 /**
 * Manages file-based storage operations for an electronic store.
 * @author Hasani
 */
public class FileHandler extends Storage{
    private Path pP;
    private Path pC;
    private Path pO;
    /**
     * Constructor for creating a FileHandler with specific file paths.
     * @param productPath The path to the product data file.
     * @param customerPath The path to the customer data file.
     * @param orderPath The path to the order data file.
     */
    public FileHandler(String productPath, String customerPath, String orderPath){
        this.pP = Paths.get(productPath);
        this.pC = Paths.get(customerPath);
        this.pO = Paths.get(orderPath);
    }

    public FileHandler(){
        this.pP = Paths.get("products.csv");
        this.pC = Paths.get("customers.csv");
        this.pO = Paths.get("orders.csv");
    }
    /**
     * Loads product data from a file into the electronic store system.
     * @throws DataException If a file IO error occurs during the load process.
     */
    @Override 
    public void loadProducts() throws DataException{
        try{
            List<String> lines = Files.readAllLines(pP);
            for(String line : lines) {
                String[] elements = line.split(",");
                if(elements.length == 15) {
                    this.electronicList.add(new Computer(
                        elements[0], elements[1], elements[2], elements[4],
                        Integer.parseInt(elements[4]), Integer.parseInt(elements[5]), Double.parseDouble(elements[6]), 
                        elements[7], Double.parseDouble(elements[8]), Double.parseDouble(elements[9]), 
                        Integer.parseInt(elements[10]), elements[11], elements[12], elements[13], Boolean.parseBoolean(elements[14])
                    ));
                }else {
                    this.electronicList.add(new Server(
                        elements[0], elements[1], elements[2], elements[4],
                        Integer.parseInt(elements[4]), Integer.parseInt(elements[5]), Double.parseDouble(elements[6]), 
                        elements[7], Double.parseDouble(elements[8]), Double.parseDouble(elements[9]), 
                        Integer.parseInt(elements[10]), elements[11], elements[12], elements[13], elements[14],
                        Double.parseDouble(elements[15]), elements[16]
                    ));
                }
            }
        }catch(IOException e) {
            throw new DataException(e);
        }
    } 
    /**
     * Loads customers data from a file into the electronic store system.
     * @throws DataException If a file IO error occurs during the load process.
     */
    @Override
    public void loadCustomers() throws DataException{
        try{
            List<String> lines = Files.readAllLines(pC);
            for(String line : lines) {
                String[] elements = line.split(",");
                this.customerList.add(new Customer(elements[0], elements[1], Integer.parseInt(elements[2]), elements[3], elements[4]));
            }
        }catch(IOException e) {
            throw new DataException(e);
        }
    }
     /**
     * Loads orders data from a file into the electronic store system.
     * @throws DataException If a file IO error occurs during the load process.
     */
    @Override 
    public void loadOrders() throws DataException {
        try{
            List<String> lines = Files.readAllLines(pO);
            for(String line : lines) {
                String[] elements = line.split(",");
                this.orderList.add(new Order(elements[0], elements[1], elements[2], Integer.parseInt(elements[3]), Double.parseDouble(elements[4])));
            }
        }catch(IOException e) {
            throw new DataException(e);
        }
    }
    /**
     * Writes product data from the electronic store system to a file.
     * @throws DataException If a file IO error occurs during the save process.
     */
    @Override
    public void writeProducts() throws DataException {
        try{
            List<String> toStringList = new ArrayList<String>();
            for(Electronics e : electronicList) {
                toStringList.add(e.getAllFields());
            }
            Files.write(this.pP, toStringList);
        }catch(IOException e) {
            throw new DataException(e);
        }
    }
    /**
     * Writes customers data from the electronic store system to a file.
     * @throws DataException If a file IO error occurs during the save process.
     */
    @Override
    public void writeCustomers() throws DataException {
        try{
            List<String> toStringList = new ArrayList<String>();
            for(Customer c : customerList) {
                toStringList.add(c.getAllFields());
            }
            Files.write(this.pP, toStringList);
        }catch(IOException e) {
            throw new DataException(e);
        }
    }
/**
     * Writes orders data from the electronic store system to a file.
     * @throws DataException If a file IO error occurs during the save process.
     */
    @Override
    public void writeOrders() throws DataException {
        try{
            List<String> toStringList = new ArrayList<String>();
            for(Order o : orderList) {
                toStringList.add(o.getAllFields());
            }
            Files.write(this.pO, toStringList);
        }catch(IOException e) {
            throw new DataException(e);
        }
    }
}
