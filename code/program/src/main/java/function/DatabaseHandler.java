package function;
import electronic_store.*;
import java.sql.*;
import java.util.Map;

import cexception.DataException;
/**
 * Handles database operations for an electronic store.
 * This class is responsible for connecting to a database and performing operations such as loading and saving products, customers, and orders.
 * @author Hasani
 */
    
public class DatabaseHandler extends Storage{
    private String userName;
    private String password;
    private String url;
    private Connection con = null;
/**
     * Constructor to create a new DatabaseHandler instance.
     * @param userName The username for the database login.
     * @param password The password for the database login.
     * @param url The URL of the database.
     * @throws DataException If a connection error occurs.
     */
    public DatabaseHandler(String userName, String password, String url) throws DataException{
        this.userName = userName;
        this.password = password;
        this.url = url;
        this.con = getConnection();
    }
    /**
     * Establishes a connection to the database.
     * @return The established database connection.
     * @throws DataException If a connection error occurs.
     */
    private Connection getConnection() throws DataException{
        Connection conn = null;
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn = DriverManager.getConnection(this.url, this.userName, this.password);
            System.out.println("Connection established");
        }catch(SQLException e) {
            e.printStackTrace();
            throw new DataException(e);
        }catch(ClassNotFoundException e) {
            System.out.println("JDBC driver not found");
        }
        return conn;
    }
    /**
     * Loads products from the database into the electronic store system.
     * @throws DataException If a database error occurs during the load process.
     */
    @Override
    public void loadProducts() throws DataException{
        this.electronicList.clear();
        try{
            String computerQuery  = "SELECT * FROM COMPUTER;";
            PreparedStatement stmt_c = this.con.prepareStatement(computerQuery);
            ResultSet rs_c = stmt_c.executeQuery();
            while(rs_c.next()) {
                electronicList.add(new Computer(
                    rs_c.getString(1),
                    rs_c.getString(2),
                    rs_c.getString(3),
                    rs_c.getString(4),
                    rs_c.getInt(5),
                    rs_c.getInt(6),
                    rs_c.getDouble(7),
                    rs_c.getString(8),
                    rs_c.getDouble(9),
                    rs_c.getDouble(10),
                    rs_c.getInt(11),
                    rs_c.getString(12),
                    rs_c.getString(13),
                    rs_c.getString(14),
                    rs_c.getBoolean(15)));
            }
            String serverQuery  = "SELECT * FROM SERVER;";
            PreparedStatement stmt_s = this.con.prepareStatement(serverQuery);
            ResultSet rs_s = stmt_s.executeQuery();
            while(rs_s.next()) {
                electronicList.add(new Server(
                    rs_s.getString(1),
                    rs_s.getString(2),
                    rs_s.getString(3),
                    rs_s.getString(4),
                    rs_s.getInt(5),
                    rs_s.getInt(6),
                    rs_s.getDouble(7),
                    rs_s.getString(8),
                    rs_s.getDouble(9),
                    rs_s.getDouble(10),
                    rs_s.getInt(11),
                    rs_s.getString(12),
                    rs_s.getString(13),
                    rs_s.getString(14),
                    rs_s.getString(15),
                    rs_s.getDouble(16),
                    rs_s.getString(17)));
            }
        }catch (SQLException e) {
            e.printStackTrace();
            throw new DataException(e);
        }
    }
    /**
     * Loads customers from the database into the electronic store system.
     * @throws DataException If a database error occurs during the load process.
     */
    @Override
    public void loadCustomers() throws DataException {
        try{
            String customerQuery = "SELECT * FROM CUSTOMER;";
            PreparedStatement stmt = this.con.prepareStatement(customerQuery);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                customerList.add(new Customer(
                    rs.getString(1),
                    rs.getString(2),
                    rs.getInt(3),
                    rs.getString(4),
                    rs.getString(5)
                ));
            }
        }catch(SQLException e) {
            e.printStackTrace();
            throw new DataException(e);
        }
    }
/**
     * Loads orders from the database into the electronic store system.
     * @throws DataException If a database error occurs during the load process.
     */
    @Override
    public void loadOrders() throws DataException{
        try{
            String orderQuery = "SELECT * FROM ORDER;";
            PreparedStatement stmt = this.con.prepareStatement(orderQuery);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                orderList.add(new Order(
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getInt(4),
                    rs.getDouble(5)
                ));
            }
        }catch(SQLException e) {
            e.printStackTrace();
            throw new DataException(e);
        }
    }
    /**
     * Writes product data from the electronic store system to the database.
     * @throws DataException If a database error occurs during the save process.
     */
    @Override
    public void writeProducts() throws DataException{
        try{
            this.con.setAutoCommit(false);
            Map map = con.getTypeMap();
            con.setTypeMap(map);
            CallableStatement stmt = null;
            String insertProcedure = "";
            for(Electronics e : electronicList) {
                if(e instanceof Computer) {
                    Computer c = (Computer)e;
                    map.put(c.getSQLTypeName(), Class.forName("Computer"));
                    insertProcedure = "{call insertComputer(?)}";
                    stmt = con.prepareCall(insertProcedure);
                    stmt.setObject(1, c);
                }else {
                    Server s = (Server)e;
                    map.put(s.getSQLTypeName(), Class.forName("Server"));
                    insertProcedure = "{call insertServer(?)}";
                    stmt = con.prepareCall(insertProcedure);
                    stmt.setObject(1, s);
                }
                stmt.execute();
                stmt.close();
            }
            this.con.commit();
        }catch(SQLException SQLe) {
            SQLe.printStackTrace();
            throw new DataException(SQLe);
        }catch(ClassNotFoundException CNFe) {
            System.out.println("class not found");
        }
    }
    /**
     * Writes Customers data from the electronic store system to the database.
     *
     * @throws DataException If a database error occurs during the save process.
     */
    @Override
    public void writeCustomers() throws DataException{
        try{
            this.con.setAutoCommit(false);
            Map map = con.getTypeMap();
            con.setTypeMap(map);
            for(Customer c : customerList) {
                map.put(c.getSQLTypeName(), Class.forName("Customer"));
                String insertProcedure = "{call insertCustomer(?)}";
                CallableStatement stmt = con.prepareCall(insertProcedure);
                stmt.setObject(1, c);
                stmt.execute();
                stmt.close();
            }
            this.con.commit();
        }catch(SQLException SQLe) {
            SQLe.printStackTrace();
            throw new DataException(SQLe);   
        }catch(ClassNotFoundException CNFe) {
            System.out.println("class not found");
        }
    }
    /**
     * Writes orders data from the electronic store system to the database.
     * @throws DataException If a database error occurs during the save process.
     */
    @Override
    public void writeOrders() throws DataException {
        try{
            this.con.setAutoCommit(false);
            Map map = con.getTypeMap();
            con.setTypeMap(map);
            for(Order o : orderList) {
                map.put(o.getSQLTypeName(), Class.forName("Order"));
                String insertProcedure = "{call insertOrder(?)}";
                CallableStatement stmt = con.prepareCall(insertProcedure);
                stmt.setObject(1, o);
                stmt.execute();
                stmt.close();
            }
        }catch(SQLException SQLe) {
            SQLe.printStackTrace();
            throw new DataException(SQLe);   
        }catch(ClassNotFoundException CNFe) {
            System.out.println("class not found");
        }
    } 
}
