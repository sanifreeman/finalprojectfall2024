package function;
import java.util.*;

import electronic_store.*;
import cexception.*;
import java.io.*;
/**
 * Abstract class representing the storage mechanism for an electronic store management system.
 */
public abstract class Storage {
    protected List<Electronics> electronicList = new ArrayList<Electronics>();
    protected List<Customer> customerList = new ArrayList<Customer>();
    protected List<Order> orderList = new ArrayList<Order>();
     /**
     * Loads and writes product,customer etc's data into the storage system.
    
     * all @throws DataException If an error occurs during data loading.
     */

    public abstract void loadProducts() throws DataException;
    public abstract void loadCustomers() throws DataException;
    public abstract void writeProducts() throws DataException;
    public abstract void writeCustomers() throws DataException;
    public abstract void loadOrders() throws DataException;
    public abstract void writeOrders() throws DataException;
    /**
     * Retrieves the list of the below  stored in the system.

     * @return A list of Order objects.
     */

    public List<Electronics> getElectronicsList() {
        return this.electronicList;
    }

    public List<Customer> getCustomerList() {
        return this.customerList;
    }

    public List<Order> getOrderList() {
        return this.orderList;
    }
    /**
     * Adds an order to the storage system.
     * @param o The order to be added.
     */
    public void addOrder(Order o) {
        this.orderList.add(o);
    }
     /**
     * Updates the storage quantity for a specific electronic product.
     * @param ID The ID of the electronic product.
     * @param quantity The quantity to be updated.
     */
    public void updateStorage(String ID, int quantity) {
        for(Electronics e : this.electronicList) {
            if(e.getID().equals(ID)) {
                e.setQuantity(e.getQuantity() - quantity);
                break;
            }
        }
    }
}
