package cexception;

/**
 * This class represents a custom exception .
 * 
 *
 * @author Hasani
 */
public class DataException extends Exception {

    /**
     * Constructs a new DataException with the specified cause.
     *
     * @param cause the cause of this exception. This is the exception that
     *              triggered this DataException.
     */
    public DataException(Throwable cause) {
        super(cause);
    }
}
