package comparer;

import electronic_store.Electronics;

/**
 * This class provides a default implementation of the Comparer interface.
 *  Comparison of two Electronics objects.
 *
 * @author Hasani
 */
public class DefaultComparer implements Comparer {

    /**
     * Compares two electronics items based on a specified order.
     * If the boolean parameter is true, the comparison is done in the natural order
     * using the first object's compareTo method. Otherwise, the comparison is reversed.
     *
     * @param e1 the first Electronics object to be compared.
     * @param e2 the second Electronics object to be compared.
     * @param r  a boolean  that determines the order of comparison.
     *           If true, e1 is compared to e2, otherwise e2 is compared to e1.
     * @return an integer representing the result of the comparison.
     *         A value of 0 indicates that e1 and e2 are equal.
     *         A value less than 0 indicates that e1 is considered less than e2, or vice versa based on the boolean.
     *         A value greater than 0 indicates that e1 is considered greater than e2, or vice versa based on the boolean.
     */
    public int compareElectronics(Electronics e1, Electronics e2, boolean r) {
        if (r) {
            return e1.compareTo(e2);
        } else {
            return e2.compareTo(e1);
        }
    }
}
