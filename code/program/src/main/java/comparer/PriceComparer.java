package comparer;

import electronic_store.*;

/**
 * This class implements the Comparer interface to compare Electronics objects based on their prices.
 * It provides a method to compare the prices of two Electronics objects either in natural or reverse order.
 *
 * @author Hasani
 */
public class PriceComparer implements Comparer {

    /**
     * Compares two electronics items based on their prices.
     * The comparison is based on the price values of the Electronics objects.
     * The order of comparison can be either natural or reverse as specified by the boolean parameter.
     *
     * @param e1 the first Electronics object to be compared based on price.
     * @param e2 the second Electronics object to be compared based on price.
     * @param r  a boolean  that determines the order of comparison.
     *           If true, the comparison is based on the natural order of prices.
     *           If false, the comparison is in reverse order of prices.
     * @return an integer representing the result of the comparison.
     *         A value of 0 indicates that the prices of e1 and e2 are equal.
     *         A value less than 0 indicates that the price of e1 is less than that of e2 or vice versa based on the boolean .
     *         A value greater than 0 indictes that the price of e1 is greater than that of e2 or vice versa based on the boolean .
     */
    public int compareElectronics(Electronics e1, Electronics e2, boolean r) {
        if (r) {
            return (int) Math.ceil(e1.getPrice() - e2.getPrice());
        } else {
            return (int) Math.ceil(e2.getPrice() - e1.getPrice());
        }
    }
}
