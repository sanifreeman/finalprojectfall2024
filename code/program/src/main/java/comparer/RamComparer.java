package comparer;

import electronic_store.Electronics;

/**
 * This class implements the Comparer interface to compare Electronics objects based on their RAM (Random Access Memory) size.
 * It provides a method to compare the RAM size of two Electronics objects either in natural or reverse order.
 *
 * @author Hasani
 */
public class RamComparer implements Comparer {

    /**
     * Compares two electronics items based on their RAM size.
     * The comparison is based on the RAM sizes of the Electronics objects.
     * The order of comparison can be either natural or reverse, as specified by the boolean parameter.
     *
     * @param e1 the first Electronics object to be compared, based on RAM size.
     * @param e2 the second Electronics object to be compared, based on RAM size.
     * @param r  a boolean  that determines the order of comparison.
     *           If true, the comparison is based on the natural order of RAM sizes.
     *           If false, the comparison is in reverse order of RAM sizes.
     * @return an integer representing the result of the comparison.
     *         A value of 0 indicates that the RAM sizes of e1 and e2 are equal.
     *         A positive value indicates that the RAM size of e1 is greater than that of e2, or vice versa based on the boolean.
     *         A negative value indicates that the RAM size of e1 is less than that of e2, or vice versa based on the boolean.
     */
    public int compareElectronics(Electronics e1, Electronics e2, boolean r) {
        if (r) {
            return (int) Math.ceil(e1.getRam() - e2.getRam());
        } else {
            return (int) Math.ceil(e2.getRam() - e1.getRam());
        }
    }
}
