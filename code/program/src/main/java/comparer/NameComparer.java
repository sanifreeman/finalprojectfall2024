package comparer;

import electronic_store.Electronics;

/**
 * This class implements the Comparer interface to compare Electronics objects based on their names.
 * It offers a way to compare the names of two Electronics objects either in original or reverse order.
 *
 * @author Hasani
 */
public class NameComparer implements Comparer {

    /**
     * Compares two electronics items based on their names.
     * The comparison is based on the  order of the names of the Electronics objects.
     * The order can be either natural or reverse, as specified by the boolean parameter.
     *
     * @param e1 the first Electronics object to be compared.
     * @param e2 the second Electronics object to be compared.
     * @param r  a boolean  that determines the order of comparison.
     *           If true, the comparison is based on the natural alphabetical order of names.
     *           If false, the comparison is in reverse alphabetical order.
     * @return an integer representing the result of the comparison.
     *         A value of 0 indicates that the names of e1 and e2 are equal.
     *         A value less than 0 indicates that the name of e1 is alphabetically less than that of e2, or vice versa based on the boolean.
     *         A value greater than 0 indicates that the name of e1 is alphabetically greater than that of e2, or vice versa based on the boolean.
     */
    public int compareElectronics(Electronics e1, Electronics e2, boolean r) {
        if (r) {
            return e1.getName().compareTo(e2.getName());
        } else {
            return e2.getName().compareTo(e1.getName());
        }
    }
}
