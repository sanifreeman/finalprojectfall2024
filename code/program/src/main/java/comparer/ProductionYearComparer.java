package comparer;

import electronic_store.Electronics;

/**
 * This class implements the Comparer interface to compare Electronics objects based on their production years.
 * It provides a method to compare the production years of two Electronics objects either in natural or reverse order.
 *
 * @author Hasani
 */
public class ProductionYearComparer implements Comparer {

    /**
     * Compares two electronics items based on their production years.
     * The comparison is based on the production year values of the Electronics objects.
     * The order of comparison can be either natural or reverse, as specified by the boolean parameter.
     *
     * @param e1 the first Electronics object to be compared, based on production year.
     * @param e2 the second Electronics object to be compared, based on production year.
     * @param r  a boolean  that determines the order of comparison.
     *           If true, the comparison is based on the natural order of production years.
     *           If false, the comparison is in reverse order of production years.
     * @return an integer representing the result of the comparison.
     *         A positive value indicates that the production year of e1 is greater than that of e2, or vice versa based on the boolean .
     *         A negative value indicates that the production year of e1 is less than that of e2, or vice versa based on the boolean .
     *         A value of 0 indicates that the production years of e1 and e2 are equal.
     */
    public int compareElectronics(Electronics e1, Electronics e2, boolean r) {
        if (r) {
            return e1.getProductionYear() - e2.getProductionYear();
        } else {
            return e2.getProductionYear() - e1.getProductionYear();
        }
    }
}
