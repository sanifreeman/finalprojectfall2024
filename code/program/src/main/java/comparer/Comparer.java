package comparer;

import electronic_store.Electronics;

/**
 * This interface defines a method for comparing two Electronics objects.
 *
 * @author Hasani
 */
public interface Comparer {

    /**
     * Compares two electronics items.
     * The comparison can be based on various attributes of the Electronics objects
     * and can be either regular or reverse based on the boolean parameter.
     *
     * @param e1 the first Electronics object to be compared.
     * @param e2 the second Electronics object to be compared.
     * @param r  a boolean  that determines the type of comparison.
     * @return an integer representing the result of the comparison.
     */
    public int compareElectronics(Electronics e1, Electronics e2, boolean r);
}
