package comparer;

import electronic_store.Electronics;

/**
 * This class implements the Comparer interface to compare Electronics objects based on their storage capacity.
 * It provides a method to compare the storage capacity of two Electronics objects either in natural or reverse order.
 *
 * @author Hasani
 */
public class StorageComparer implements Comparer {

    /**
     * Compares two electronics items based on their storage capacity.
     * The comparison is based on the storage capacities of the Electronics objects.
     * The order of comparison can be either natural or reverse, as specified by the boolean parameter.
     *
     * @param e1 the first Electronics object to be compared, based on storage capacity.
     * @param e2 the second Electronics object to be compared, based on storage capacity.
     * @param r  a boolean that determines the order of comparison.
     *           If true, the comparison is based on the natural order of storage capacities.
     *           If false, the comparison is in reverse order of storage capacities.
     * @return an integer representing the result of the comparison.
     *         A value of 0 indicates that the storage capacities of e1 and e2 are equal.
     *         A positive value indicates that the storage capacity of e1 is greater than that of e2, or vice versa based on the boolean.
     *         A negative value indicates that the storage capacity of e1 is less than that of e2, or vice versa based on the boolean.
     */
    public int compareElectronics(Electronics e1, Electronics e2, boolean r) {
        if (r) {
            return (int) Math.ceil(e1.getStorage() - e2.getStorage());
        } else {
            return (int) Math.ceil(e2.getStorage() - e1.getStorage());
        }
    }
}
