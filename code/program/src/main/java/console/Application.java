package console;
import function.*;
import java.text.*;
import java.util.*;
import java.util.logging.FileHandler;

import cexception.DataException;
import electronic_store.*;
/**
 * Abstract  class for applications managing electronic store services.
 * Provides common functionalities like loading data, searching products, and creating orders.
 *
 * @author Tianrui
 */
public abstract class Application {
    protected Scanner sc = new Scanner(System.in);
    protected Storage storage;

    /**
     * Loads data from either  existing database or from a file based on user input.
     * User can choose between loading from a database ('D') or a file ('F').
     * Throws an exception if the input is invalid more than three times.
     *
     * @throws DataException If there is an issue with data loading or invalid input.
     */
    public void loadData() throws DataException{
        System.out.println("Welcome to use Electronics store service");
        int invalidCount = 0;
        while(invalidCount < 3) {
            System.out.println("Would you like to load data from existing database or from file?" +  
                "enter 'D' for database; 'F' for filie");
            String loadOption = sc.next();
            sc.nextLine();
            if(loadOption.equalsIgnoreCase("D")) {
                loadFromDatabase();
                break;
            }else if(loadOption.equalsIgnoreCase("F")) {
                loadFromFile();
                break;
            }else{
                System.out.println("Invalid Input");
                invalidCount ++;
            }
        }
        throw new DataException(new IllegalAccessError());
    }
    /**
     * Loads data from a specified database.
     * Prompts the user for database credentials including username, password, and URL.
     *
     * @throws DataException If there is an issue with database connectivity or data loading.
     */
    private void loadFromDatabase() throws DataException {
        System.out.println("Enter userName: ");
        String userName = sc.nextLine();
        System.out.println("Enter password: ");
        String password = sc.nextLine();
        System.out.println("Enter url for the database: ");
        String url = sc.nextLine();
        this.storage = new DatabaseHandler(userName, password, url);
    }
    /**
     * Loads data from a file using the FileHandler.
     * This method is used when the user chooses to load data from a file.
     *
     * @throws DataException If there is an issue with file reading or data loading.
     */
    public void loadFromFile() throws DataException{
            this.storage = new FileHandler();
            System.out.println("Files loaded");
    }

   /**
     * Presents various task options to the user, including searching for products and creating orders.
     * The user can also enter storage management for administrative purposes.
     *
     * @throws DataException If an invalid task is selected or there is an issue with task processing.
     */
    public void taskOption() throws DataException{
        System.out.println("Enter task number: (1)Search Product, (2)Create Transaction(Order)" + 
            "press enter to enter storage management (admin)");
        String taskSelect = sc.next();
        sc.nextLine();
        if(taskSelect.equals("1")) {
            searchProducts();
            System.out.println("Would u like to create order? Y/N"); 
            String creatOption = sc.next();
            sc.nextLine();
            if(creatOption.equalsIgnoreCase("Y")) {
                createOrder();
            }else{
                System.out.println("Thank you for using this system.");
                System.exit(0);
            }
        }else if(taskSelect.equals("2")) {
            createOrder();
        }
    }
    /**
     * Allows the user to search for products based on different criteria like name, price, production year, RAM, or storage.
     * Also provides an option to display all products.
     */
    private void searchProducts() {
        System.out.println("How would you like to search the products? "+
            "(1)Name, (2)Price, (3)Production Year, (4)Ram, (5)Storage, (6)Display All");
        TypeEnum te = TypeEnum.DEFAULT;
        String searchOption = sc.next();
        sc.nextLine();
        String searchStr = "";
        if(searchOption.equals("1")) {
            te = TypeEnum.NAME;
            System.out.println("Enter the name you would like to search: ");
            searchStr = sc.nextLine();
        }else if(searchOption.equals("2")) {
            te = TypeEnum.PRICE;
            System.out.println("Enter the price you would like to search: ");
            searchStr = sc.nextLine();
        }else if(searchOption.equals("3")) {
            te = TypeEnum.PRICE;
            System.out.println("Enter the Production Year you would like to search: ");
            searchStr = sc.nextLine();
        }else if(searchOption.equals("4")) {
            te = TypeEnum.PRICE;
            System.out.println("Enter the amount of Ram you would like to search: ");
            searchStr = sc.nextLine();
        }else if(searchOption.equals("5")) {
            te = TypeEnum.PRICE;
            System.out.println("Enter the amount of Storage you would like to search: ");
            searchStr = sc.nextLine();
        }
        Display search = new Querying(storage, te, searchStr);
        search.displayProducts();
    }
    /**
     * Creates a new order based on user input.
     * The user is prompted to enter product ID, quantity, and customer ID.
     * The method also handles inventory verification and price calculation after promotion.
     * @throws DataException If there is an issue with order creation, such as insufficient inventory or invalid product/customer IDs.
     */
    private void createOrder() throws DataException{
        System.out.println("Enter the productID: ");
        String productID = sc.nextLine();
        System.out.println("Enter the amount you would like to purchase: ");
        int quantity = sc.nextInt();
        sc.nextLine();
        Electronics product = null;
        Customer customer = null;
        for(Electronics e : storage.getElectronicsList()) {
            if(e.getID().equalsIgnoreCase(productID)) {
                product = e;
                if(!e.verifyQuantity(quantity)) {
                    throw new DataException(new IllegalArgumentException("not enough inventory"));
                }
            }
        }
        System.out.println("Enter the customerID");
        String customerID = sc.nextLine();
        for(Customer c : storage.getCustomerList()) {
            if(customerID.equalsIgnoreCase(c.getID())) {
                customer = c;
            }
        }
        double price = getPriceAfterPromotion(customer, quantity * product.getPrice());
        try{
            Order o = new Order("O" + getDate(), customerID, product.getID(), quantity, price);
            storage.addOrder(o);
            storage.updateStorage(product.getID(), quantity);
        }catch(NullPointerException e) {
            throw new DataException(e);
        }
    }
    /**
     * Gets the current date and time in a specific format (yyyy/mm/dd-hh:mm:ss).
     *
     * @return A string representing the current date and time.
     */
    public String getDate() {
        Date date = Calendar.getInstance().getTime();  
        DateFormat dateFormat = new SimpleDateFormat("yyyy/mm/dd-hh:mm:ss");  
        return dateFormat.format(date);
    }
    /**
     * Calculates the price after applying any applicable promotions for a given customer.
     *
     * @param c The customer for whom the promotion is being applied.
     * @param price The original price before applying the promotion.
     * @return The price after applying the promotion.
     */
    public double getPriceAfterPromotion(Customer c, double price) {
        Promotion p = new Promotion();
        p.testApplicablePromotion(c);
        return p.getPromotionMultiplier() * price;
    }
}
