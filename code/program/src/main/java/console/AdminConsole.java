package console;

import function.*;
import cexception.DataException;
import electronic_store.*;
import java.util.*;
import java.util.logging.FileHandler;

import com.apple.eawt.Application;
/**
 * This class is  an administration console for the  electronic store .
 * It extends the Application class and provides functionality to manage products, customers, and orders through a console interface.
 *
 * @author Tianrui
 */
public class AdminConsole extends Application{
    StorageManager sm = new StorageManager(storage);
    /**
     * The main method to start the administration console.
     * It handles user inputs for various administrative tasks.
     *
     * @throws DataException If there is an issue with data processing.
     */
    public static void main(String[] args) throws DataException{
        AdminConsole admin = new AdminConsole();
        Scanner sc = new Scanner(System.in);
        admin.loadData();
        boolean moreOperation = false;
        do{
            moreOperation = false;
            admin.taskOption();
            System.out.println("Would you like to perform more operation? Enter Y/N");
            String more = sc.next();
            sc.nextLine();
            if(more.equalsIgnoreCase("Y")) {
                moreOperation = true;
            }
        }while(moreOperation);
    }
    /**
     * Loads data from  file, providing an option to customize the file path.
     * If the user chooses to customize the path, they can specify paths for products, customers, and orders.
     * @throws DataException If there is an issue in loading data from the file.
     */

    @Override
    public void loadFromFile() throws DataException {
        System.out.println("Would you like to customize path or keep the default?" + 
        "Enter 'c' for customize else press enter for default");
        String pathOption = sc.nextLine();
        if(pathOption.equalsIgnoreCase("c")) {
            System.out.println("Enter the path to products: ");
            String pPath = sc.nextLine();
            System.out.println("Enter the path to customers: ");
            String cPath = sc.nextLine();
            System.out.println("Enter the path to orders");
            String oPath = sc.nextLine();
            this.storage = new FileHandler(pPath, cPath, oPath);
        }else {
            this.storage = new FileHandler();
        }
    }
    /**
     * Displays options for various tasks and handles user input to perform operations on products, customers, or orders.
     * Throws an exception for invalid input.
     *
     * @throws DataException If an invalid input is provided.
     */
    @Override
    public void taskOption() throws DataException {
        super.taskOption();
        System.out.println("What table would you like to perform the operation?" + 
            "(1)Products, (2)Customers, (3)Orders");
        String taskSelect = sc.next();
        if(taskSelect.equals("1")) {
            operateProducts(); 
        }else if(taskSelect.equals("2")) {
            operateCustomers();
        }else if(taskSelect.equals("3")) {
            operateOrders();
        }else {
            System.out.println("invalid input");
            throw new DataException(new IllegalArgumentException("invalid input"));
        }
    }
    /**
     * Handles operations related to products such as modifying product details or deleting a product.
     * Prompts the user for the product ID and the specific operation to perform.
     *
     * @throws DataException If there is an issue with product data processing.
     */
    private void operateProducts() throws DataException {
        System.out.println("Enter the product ID"); 
        String productID = sc.nextLine();
        System.out.println("Enter the number that corresponds to the operation would you like to perform: \n" + 
            "Modify: (1)quantity, (2)Name, (3)Brand, (4)Category, (5)Production Year, (6)Warranty, (7)Price, (8)Description, (9)Storage, (10)Ram \n" +
            "(11)CPU(Computer), (12)System(Computer), (13)Graphic Card(Computer), (14)Portable(Computer) \n" + 
            "(15)SCPU(Server), (16)Storage System(Server), (17)Calculation GPU(Server), (18)Port(Server), (19)Bandwidth(Server), (20)Raid(Server) \n" +
            "Or 0 to delete an product");
        String operation = sc.next();
        sc.nextLine();
        switch(operation) {
            case "1":
                System.out.println("Enter the new quantity: "); 
                int quantity = sc.nextInt();
                sc.nextLine();
                sm.modifyQuantity(productID, quantity);
                break;
            case "2":
                System.out.println("Enter the new name: "); 
                String name = sc.nextLine();
                sm.modifyName(productID, name);
                break;
            case "3":
                System.out.println("Enter the new brand: "); 
                String brand = sc.nextLine();
                sm.modifyName(productID, brand);
                break;
            case "4":
                System.out.println("Enter the new category: "); 
                String category = sc.nextLine();
                sm.modifyName(productID, category);
                break;
            case "5":
                System.out.println("Enter the new production year: "); 
                int py = sc.nextInt();
                sc.nextLine();
                sm.modifyProductionYear(productID, py);
                break;
            case "6":
                System.out.println("Enter the new warranty: "); 
                int warranty = sc.nextInt();
                sc.nextLine();
                sm.modifyWarranty(productID, warranty);
                break;
            case "7":
                System.out.println("Enter the new price: ");
                double price = sc.nextDouble();
                sc.nextLine();
                sm.modifyPrice(productID, price);
                break;
            case "8":
                System.out.println("Enter the new description: "); 
                String description = sc.nextLine();
                sm.modifyDescription(productID, description);
                break;
            case "9":
                System.out.println("Enter the new storage: ");
                double storage = sc.nextDouble();
                sc.nextLine();
                sm.modifyStorage(productID, storage);
                break;
            case "10":
                System.out.println("Enter the new ram: ");
                double ram = sc.nextDouble();
                sc.nextLine();
                sm.modifyRam(productID, ram);
                break;
            //Computer
            case "11":
                System.out.println("Enter the new CPU: "); 
                String CPU = sc.nextLine();
                sm.modifyCPU(productID, CPU);
                break;
            case "12":
                System.out.println("Enter the new system: "); 
                String system = sc.nextLine();
                sm.modifyCSystem(productID, system);
                break;
            case "13":
                System.out.println("Enter the new graphic card: "); 
                String graphicCard = sc.nextLine();
                sm.modifyCGraphicCard(productID, graphicCard);
                break;
            case "14":
                System.out.println("Enter the new portable property: ");
                boolean portable = sc.nextBoolean();
                sm.modifyCPortable(productID, portable);
            //Server
            case "15":
                System.out.println("Enter the new SCPU: "); 
                String SCPU = sc.nextLine();
                sm.modifySSCPU(productID, SCPU);
                break;
            case "16":
                System.out.println("Enter the new storage system: "); 
                String storageSystem = sc.nextLine();
                sm.modifySStorageSystem(productID, storageSystem);
                break;
            case "17":
                System.out.println("Enter the new calculation GPU: "); 
                String calculationGPU = sc.nextLine();
                sm.modifySCalculationGPU(productID, calculationGPU);
                break;
            case "18":
                System.out.println("Enter the new port: "); 
                String port = sc.nextLine();
                sm.modifySPort(productID, port);
                break;
            case "19":
                System.out.println("Enter the new bandwidth: ");
                double bandwidth = sc.nextDouble();
                sm.modifySBandwidth(productID, bandwidth);
                break;
            case "20":
                System.out.println("Enter the new Raid config: "); 
                String raid = sc.nextLine();
                sm.modifySRaid(productID, raid);
                break;
            case "0":
                sm.deleteProduct(productID);
                break;
            default:
                System.out.println("Invalid input");
                break;
        }
    }
    /**
     * Manages customer-related operations like viewing the customer list, modifying customer details, or deleting a customer.
     * Prompts the user for the customer ID and the specific operation to perform.
     *
     * @throws DataException If there is an issue with customer data processing.
     */
    private void operateCustomers() throws DataException {
        System.out.println("Would you like to see the entire list of customers? Enter Y/N");
        String showCustomers = sc.next();
        sc.nextLine();
        if(showCustomers.equalsIgnoreCase("y")) {
            for(Customer c : storage.getCustomerList()) {
                System.out.println(c);
            }
        }
        System.out.println("Enter the customer ID to operate: ");
        String customerID = sc.nextLine();
        System.out.println("Enter the number that corresponds to the operation would you like to perform: \n" + 
            "Modify: (1)name, (2)age, (3)email, (4)address \n" + 
            "Or 0 to delete an customer");
        String operation = sc.next();
        sc.nextLine();
        switch(operation) {
            case "1":
                System.out.println("Enter the new name: ");
                String name = sc.nextLine();
                sm.modifyCName(customerID, name);
                break;
            case "2":
                System.out.println("Enter the new age: ");
                int age = sc.nextInt();
                sc.nextLine();
                sm.modifyCAge(customerID, age);
                break;
            case "3":
                System.out.println("Enter the new email: ");
                String email = sc.nextLine();
                sm.modifyCEmail(customerID, email);
                break;
            case "4":
                System.out.println("Enter the new address: ");
                String address = sc.nextLine();
                sm.modifyCAddress(customerID, address);
                break;
            default:
                System.out.println("Invalid input");
                break;
        }
    }
     /**
     * Handles order-related operations including displaying all orders, adding a new order, or deleting an order.
     * Prompts the user for the necessary details based on the selected operation.
     *
     * @throws DataException If there is an issue with order data processing.
     */
    private void operateOrders() throws DataException {
        System.out.println("Enter number that corresponds to the operation would you like to perform: \n" + 
            "(1)Display all orders, (2)add order, (3)delete order");
        String operation = sc.next();
        sc.nextLine();
        if(operation.equals("1")) {
            for(Order o : storage.getOrderList()) {
                System.out.println(o);
            }
        }else if(operation.equals("2")) {
            System.out.println("Enter orderID");
            String orderID = sc.nextLine();
            System.out.println("Enter customerID");
            String customerID = sc.nextLine();
            System.out.println("Enter productID");
            String productID = sc.nextLine();
            System.out.println("Enter quantity");
            int quantity = sc.nextInt();
            sc.nextLine();
            System.out.println("Enter price");
            double price = sc.nextDouble();
            sc.nextLine();
            sm.addOrder(new Order(orderID, customerID, productID, quantity, price));
        }else if(operation.equals("3")) {
            System.out.println("Enter orderID: ");
            String orderID = sc.nextLine();
            sm.deleteOrder(orderID);
        }else {
            System.out.println("Invalid input");
        }
    }
}
