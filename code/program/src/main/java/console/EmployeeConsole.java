package console;
import java.util.*;

import cexception.DataException;
/**
 * This class represents a console interface specifically for employees in an electronic store management system.
 * It extends the Application class and provides functionalities tailored to employee operations.
 *
 * @author Tianrui 
 */

public class EmployeeConsole extends Application{
    /**
     * The main method to start the employee console.
     * It handles user inputs for various tasks specific to employees and provides the option to perform more operations.
     * @throws DataException If there is an issue with data processing or task execution.
     */
    public static void main(String[] args) throws DataException{
        Scanner sc = new Scanner(System.in);
        EmployeeConsole employee = new EmployeeConsole();
        employee.loadData();
        boolean moreOperation = false;
        do{
            moreOperation = false;
            employee.taskOption();
            System.out.println("Would you like to perform more operation? Enter Y/N");
            String more = sc.next();
            sc.nextLine();
            if(more.equalsIgnoreCase("Y")) {
                moreOperation = true;
            }
        }while(moreOperation);
    }
}
