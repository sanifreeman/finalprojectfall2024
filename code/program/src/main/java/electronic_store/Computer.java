package electronic_store;

import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
/**
 * Represents a computer product in an electronic store.
 * This class extends the Electronics class and includes specific properties such as CPU etc .
 * It also provides SQL d capabilities for database interaction.
 *
 * @author Tianrui
 */
public class Computer extends Electronics{
    private String CPU;
    private String system;
    private String graphicCard;
    private boolean portable;
    private String typeName = "Computer_typ";
/**
     * Constructor to create a new Computer instance with detailed specifications.
     *
     * @param name The name of the computer.
     * @param ID The ID of the computer.
     * @param brand The brand of the computer.
     * @param category The category of the computer.
     * @param productionYear The production year of the computer.
     * @param warranty The warranty period of the computer.
     * @param price The price of the computer.
     * @param description A description of the computer.
     * @param storage The storage capacity of the computer.
     * @param ram The RAM size of the computer.
     * @param quantity The available quantity of the computer.
     * @param CPU The CPU specification of the computer.
     * @param system The operating system of the computer.
     * @param graphicCard The graphic card specification of the computer.
     * @param portable The portability status ( of the computer.
     */

    public Computer(
        String name, String ID, String brand, String category, 
        int productionYear, int warranty, double price, 
        String description, double storage, double ram,
        int quantity, String CPU, String system, String graphicCard, boolean portable) {
        super(name, ID, brand, category, productionYear, 
        warranty, price, description, storage, ram, quantity);
        this.CPU = CPU;
        this.system = system;
        this.graphicCard = graphicCard;
        this.portable = portable;
    }
    /**
     * Copy constructor to create a new Computer instance by copying another.
     *
     * @param other The Computer instance to be copied.
     */
    public Computer(Computer other) {
        this(other.getName(), other.getID(), other.getBrand(), 
            other.getCategory(), other.getProductionYear(), 
            other.getWarranty(), other.getPrice(), other.getDescription(),
            other.getStorage(), other.getRam(), other.getQuantity(), 
            other.CPU, other.system, other.graphicCard, other.portable);
    }
    // Getters and setters for each attribute
    public String getCPU() {
        return this.CPU;
    }

    public void setCPU(String CPU) {
        this.CPU = CPU;
    }

    public String getSystem() {
        return this.system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getGraphicCard() {
        return this.graphicCard;
    }

    public void setGraphicCard(String graphicCard) {
        this.graphicCard = graphicCard;
    }

    public boolean isPortable() {
        return this.portable;
    }

    public boolean getPortable() {
        return this.portable;
    }

    public void setPortable(boolean portable) {
        this.portable = portable;
    }

    public String getTypeName() {
        return this.typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
    /**
     * Returns a string representation of the Computer instance, including all attributes.
     *
     * @return A string representation of the Computer.
     */
    @Override
    public String toString() {
        return
            super.toString() + 
            ", CPU: " + this.CPU + 
            ", system: " + this.system + 
            ", graphicCard: " + this.graphicCard + 
            ", portable: " + this.portable;
    }
    /**
     * Compares this Computer instance with another Electronics instance.
     * Comparison is based on various attributes of the Electronics.
     *
     * @param e The Electronics instance to compare with.
     * @return An integer with result.
     */
    @Override
    public int compareTo(Electronics e) {
        Computer o = (Computer)e;
        if(!this.getID().equals(o.getID())) {
            return this.getID().compareTo(o.getID());
        }
        if(!this.getName().equals(o.getName())) {
            return this.getName().compareTo(o.getName());
        }
        if(!this.getBrand().equals(o.getBrand())) {
            return this.getBrand().compareTo(o.getBrand());
        }
        if(!this.getCategory().equals(o.getCategory())) {
            return this.getCategory().compareTo(o.getCategory());
        }
        if(this.getProductionYear() != o.getProductionYear()) {
            return this.getProductionYear() - o.getProductionYear();
        }
        if(this.getWarranty() != o.getWarranty()) {
            return this.getWarranty() - o.getWarranty();
        }
        if(this.getPrice() != o.getPrice()) {
            return (int)Math.ceil(this.getPrice() - o.getPrice());
        }
        if(this.getStorage() != o.getStorage()) {
            return (int)Math.ceil(this.getStorage() - o.getStorage());
        }
        if(this.getRam() != o.getRam()) {
            return (int)Math.ceil(this.getRam() - o.getRam());
        }
        if(!this.CPU.equals(o.CPU)) {
            return this.CPU.compareTo(o.CPU);
        }
        if(!this.graphicCard.equals(o.graphicCard)) {
            return this.graphicCard.compareTo(o.graphicCard);
        }
        return this.getQuantity() - o.getQuantity();
    }
    /**
     * Provides the SQL type name of this object.
     * @return The SQL type name.
     * @throws SQLException If an SQL error occurs.
     */
    @Override
    public String getSQLTypeName() throws SQLException{
        return this.typeName;
    }
    /**
     * Reads data from an SQL input stream to make the attributes of this Computer instance.
     * @param stream The SQLInput stream from which to read.
     * @param typeName The SQL type name of this object.
     * @throws SQLException If an SQL error occurs during reading.
     */
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setTypeName(typeName);
        setName(stream.readString());
        setID(stream.readString());
        setBrand(stream.readString());
        setCategory(stream.readString());
        setProductionYear(stream.readInt());
        setWarranty(stream.readInt());
        setPrice(stream.readDouble());
        setDescription(stream.readString());
        setStorage(stream.readDouble());
        setRam(stream.readDouble());
        setQuantity(stream.readInt());
        setCPU(stream.readString());
        setSystem(stream.readString());
        setGraphicCard(stream.readString());
        setPortable(stream.readBoolean());
    }
    /**
     * Writes this Computer instances data to an SQL output stream.
     * @param stream The SQLOutput stream to which to write.
     * @throws SQLException If an SQL error occurs during writing.
     */
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(getName());
        stream.writeString(getID());
        stream.writeString(getBrand());
        stream.writeString(getCategory());
        stream.writeInt(getProductionYear());
        stream.writeInt(getWarranty());
        stream.writeDouble(getPrice());
        stream.writeString(getDescription());
        stream.writeDouble(getStorage());
        stream.writeDouble(getRam());
        stream.writeInt(getQuantity());
        stream.writeString(getCPU());
        stream.writeString(getSystem());
        stream.writeString(getGraphicCard());
        stream.writeBoolean(getPortable());
    }
     /**
     * Retrieves a string containing all field values of this Computer instance kind of like a getter.
     * @return A string with all field values.
     */
    public String getAllFields() {
        return 
            super.getAllFields() + "," +
            getCPU() + "," + 
            getSystem() + "," + 
            getGraphicCard() + "," + 
            getPortable();
    }
}
