package electronic_store;

import java.lang.reflect.Constructor;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
/**
 * Represents a server product in an electronic store.
 * This class extends the Electronics class 
 */
public class Server extends Electronics{
    private String SCPU;
    private String storageSystem;
    private String calculationGPU;
    private String port;
    private double bandwidth;
    private String raid;
    private String typeName = "Server_typ";
   /**
     * Constructor to create a new Server instance with detailed specifications.
     *
     * @param name The name of the server.
     * @param ID The ID of the server.
     * @param brand The brand of the server.
     * @param category The category of the server.
     * @param productionYear The production year of the server.
     * @param warranty The warranty period of the server.
     * @param price The price of the server.
     * @param description A description of the server.
     * @param storage The storage capacity of the server.
     * @param ram The RAM size of the server.
     * @param quantity The available quantity of the server.
     * @param SCPU The CPU specification of the server.
     * @param storageSystem The storage system used in the server.
     * @param calculationGPU The GPU used for calculations in the server.
     * @param port The port configuration of the server.
     * @param bandwidth The network bandwidth of the server.
     * @param raid The RAID configuration of the server.
     */
    public Server(String name, String ID, String brand, String category, 
        int productionYear, int warranty, double price, String description, 
        double storage, double ram, int quantity, String SCPU, String storageSystem, 
        String calculationGPU, String port, double bandwidth, String raid) {
        super(name, ID, brand, category, productionYear, 
            warranty, price, description, storage, ram, quantity);
        if(bandwidth <= 0) {
            throw new IllegalArgumentException("bandwith could not be less than 0 (or set to 0)");
        }
        this.SCPU = SCPU;
        this.storageSystem = storageSystem;
        this.calculationGPU = calculationGPU;
        this.port = port;
        this.bandwidth = bandwidth;
        this.raid = raid;
    }
    /**
     * Copy constructor to create a new Server instance by copying another.
     * @param other The Server instance to be copied.
     */
    public Server(Server other) {
        this(other.getName(), other.getID(), other.getBrand(), 
            other.getCategory(), other.getProductionYear(), 
            other.getWarranty(), other.getPrice(), other.getDescription(),
            other.getStorage(), other.getRam(), other.getQuantity(), 
            other.SCPU, other.storageSystem, other.calculationGPU, other.port, other.bandwidth, other.raid);
    }
        // Getters and setters for each attribute
    public String getSCPU() {
        return this.SCPU;
    }

    public void setSCPU(String SCPU) {
        this.SCPU = SCPU;
    }

    public String getStorageSystem() {
        return this.storageSystem;
    }

    public void setStorageSystem(String storageSystem) {
        this.storageSystem = storageSystem;
    }

    public String getCalculationGPU() {
        return this.calculationGPU;
    }

    public void setCalculationGPU(String calculationGPU) {
        this.calculationGPU = calculationGPU;
    }

    public String getPort() {
        return this.port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public double getBandwidth() {
        return this.bandwidth;
    }

    public void setBandwidth(double bandwidth) {
        this.bandwidth = bandwidth;
    }

    public String getRaid() {
        return this.raid;
    }

    public void setRaid(String raid) {
        this.raid = raid;
    }

    public String getTypeName() {
        return this.typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
   // to string similar to description in other classes
    public String toString() {
        return 
            super.toString() +
            ", SCPU='" + getSCPU() + "'" +
            ", Storage System: " + getStorageSystem() + 
            ", Calculation GPU: " + getCalculationGPU() + 
            ", Port: " + getPort() + 
            ", Bandwidth: " + getBandwidth() + 
            ", Raid: " + getRaid();
    }
    /**
     * Compares this Server instance with another Electronics instance.
     * @param e The Electronics instance to compare with.
     * @return An integer indicating the comparison result.
     */
    @Override
    public int compareTo(Electronics e) {
        Server o = (Server)e;
        if(!this.getID().equals(o.getID())) {
            return this.getID().compareTo(o.getID());
        }
        if(!this.getName().equals(o.getName())) {
            return this.getName().compareTo(o.getName());
        }
        if(!this.getBrand().equals(o.getBrand())) {
            return this.getBrand().compareTo(o.getBrand());
        }
        if(!this.getCategory().equals(o.getCategory())) {
            return this.getCategory().compareTo(o.getCategory());
        }
        if(this.getProductionYear() != o.getProductionYear()) {
            return this.getProductionYear() - o.getProductionYear();
        }
        if(this.getWarranty() != o.getWarranty()) {
            return this.getWarranty() - o.getWarranty();
        }
        if(this.getPrice() != o.getPrice()) {
            return (int)Math.ceil(this.getPrice() - o.getPrice());
        }
        if(this.getStorage() != o.getStorage()) {
            return (int)Math.ceil(this.getStorage() - o.getStorage());
        }
        if(this.getRam() != o.getRam()) {
            return (int)Math.ceil(this.getRam() - o.getRam());
        }
        if(!this.SCPU.equals(o.SCPU)) {
            return this.SCPU.compareTo(o.SCPU);
        }
        if(!this.calculationGPU.equals(o.calculationGPU)) {
            return this.calculationGPU.compareTo(o.calculationGPU);
        }
        if(this.bandwidth != o.bandwidth) {
            return (int)(this.bandwidth - o.bandwidth);
        }
        return this.getQuantity() - o.getQuantity();
    }
    /**
     * Provides the SQL type name of this object.
     * @return The SQL type name.
     * @throws SQLException If an SQL error occurs.
     */
    @Override
    public String getSQLTypeName() throws SQLException{
        return this.typeName;
    }
    /**
     * Reads data from an SQL input stream .
     * @param stream The SQLInput stream from which to read.
     * @param typeName The SQL type name of this object.
     * @throws SQLException If an SQL error occurs during reading.
     */
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setTypeName(typeName);
        setName(stream.readString());
        setID(stream.readString());
        setBrand(stream.readString());
        setCategory(stream.readString());
        setProductionYear(stream.readInt());
        setWarranty(stream.readInt());
        setPrice(stream.readDouble());
        setDescription(stream.readString());
        setStorage(stream.readDouble());
        setRam(stream.readDouble());
        setQuantity(stream.readInt());
        setSCPU(stream.readString());
        setStorageSystem(stream.readString());
        setCalculationGPU(stream.readString());
        setPort(stream.readString());
        setBandwidth(stream.readDouble());
        setRaid(stream.readString());
    }
     /**
     * Writes this Server instance's data to an SQL output stream.
     * @param stream The SQLOutput stream to which to write.
     * @throws SQLException If an SQL error occurs during writing.
     */
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(getName());
        stream.writeString(getID());
        stream.writeString(getBrand());
        stream.writeString(getCategory());
        stream.writeInt(getProductionYear());
        stream.writeInt(getWarranty());
        stream.writeDouble(getPrice());
        stream.writeString(getDescription());
        stream.writeDouble(getStorage());
        stream.writeDouble(getRam());
        stream.writeInt(getQuantity());
        stream.writeString(getSCPU());
        stream.writeString(getStorageSystem());
        stream.writeString(getCalculationGPU());
        stream.writeString(getPort());
        stream.writeDouble(getBandwidth());
        stream.writeString(getRaid());
    }
    /**
     * Retrieves a string containing all field values of this Server instance.
     * @return A string with all field values.
     */
    public String getAllFields() {
        return 
            super.getAllFields() + "," +
            getSCPU() + "," +
            getStorageSystem() + "," +
            getCalculationGPU() + "," +
            getPort() + "," +
            getBandwidth() + "," +
            getRaid();
    }
}
