package electronic_store;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
/**
 * Represents a customer in an electronic store.
 * It implements the SQLData interface to facilitate database interactions.
 * @author Tianrui
 */
public class Customer implements SQLData{

    private String name;
    private String ID;
    private int age;
    private String email;
    private String address;
    private String typeName = "Customer_typ";
    /**
     * Constructor to create a new Customer.
     * @param name The name of the customer.
     * @param ID The unique ID of the customer.
     * @param age The age of the customer.
     * @param email The email address of the customer.
     * @param address The physical address of the customer.
     */
    public Customer(
        String name,
        String ID,
        int age,
        String email,
        String address
    ) {
        this.name = name;
        this.ID = ID;
        this.age = age;
        this.email = email;
        this.address = address;
    }
    // Getters and setters for each attribute
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getID() {
        return this.ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        if (age < 18) {
            throw new IllegalArgumentException("Age should be greater or equals to 18 to become a customer");
        }
        this.age = age;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTypeName() {
        return this.typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
    /**
     * Returns a string representation of the Customer.
     *
     * @return A string representation of the Customer.
     */

    @Override
    public String toString() {
        return 
            " name: " + getName() + 
            ", ID: " + getID() + 
            ", age: " + getAge() + 
            ", email: " + getEmail() + 
            ", address: " + getAddress() + 
            ", typeName: " + getTypeName();
    }
    /**
     * Provides the SQL type name of this object.
     * @return The SQL type name.
     * @throws SQLException If an SQL error occurs.
     */
    @Override
    public String getSQLTypeName() {
        return this.typeName;
    }
    /**
     * Reads data from an SQL input stream.
     * @param stream The SQLInput stream from which to read.
     * @param typeName The SQL type name of this object.
     * @throws SQLException If an SQL error occurs during reading.
     */
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException{
        setTypeName(typeName);
        setName(stream.readString());
        setID(stream.readString());
        setAge(stream.readInt());
        setEmail(stream.readString());
        setAddress((stream.readString()));
    }
    /**
     * Writes this Customers data to an SQL output stream.
     * @param stream The SQLOutput stream to which to write.
     * @throws SQLException If an SQL error occurs during writing.
     */
    @Override public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(getName());
        stream.writeString(getID());
        stream.writeInt(getAge());
        stream.writeString(getEmail());
        stream.writeString(getAddress());
    }
    /**
     * Retrieves a string containing all field values of this Customer instance.
     * @return A string with all field values.
     */
    public String getAllFields() {
        return 
            getName() + "," +
            getID() + "," +
            getAge() + "," +
            getEmail() + "," +
            getAddress();
    }
}
