package electronic_store;

import java.sql.*;
/**
 * Abstract class representing a  electronic item in an electronic store.
 * It  implements Comparable for sorting and comparing electronics based on certain criteria.
 * @author Tianrui and Hasani
 */

public abstract class Electronics implements Comparable<Electronics>, SQLData{
    private String name;
    private String ID;
    private String brand;
    private String category;
    private int productionYear;
    private int warranty;
    private double price;
    private String description;
    private double storage;
    private double ram;
    private int quantity;
    
   /**
    * Constructor for creating an Electronics object with detailed attributes.
    *
    * @param name The name of the electronic item.
    * @param ID The unique ID of the item.
    * @param brand The brand of the item.
    * @param category The category of the item.
    * @param productionYear The production year of the item.
    * @param warranty The warranty period in years.
    * @param price The price of the item.
    * @param description A description of the item.
    * @param storage The storage capacity in GB.
    * @param ram The RAM size in GB.
    * @param quantity The available quantity.
    */
    public Electronics(String name, String ID, String brand, String category, 
        int productionYear, int warranty, double price, String description, 
        double storage, double ram, int quantity) {
        if(price <= 0 || storage <= 0 || ram <= 0) {
            throw new IllegalArgumentException("price or storage or ram could not be lower than 0 (or set to 0)");
        }
        if(productionYear < 1800) {
            throw new IllegalArgumentException("production year should not before 1850");
        }
        if(quantity < 0) {
            throw new IllegalArgumentException("quantity may not be negative");
        }
        this.name = name;
        this.ID = ID;
        this.brand = brand;
        this.category = category;
        this.productionYear = productionYear;
        this.warranty = warranty;
        this.price = price;
        this.description = description;
        this.storage = storage;
        this.ram = ram;
        this.quantity = quantity;
    }
    /**
     * Copy constructor for creating a new Electronics object from another Electronics instance.
     *
     * @param other The Electronics instance to copy.
     */
    public Electronics(Electronics other) {
        this(other.name, other.ID, other.brand, other.category, other.productionYear, 
        other.warranty, other.price, other.description, other.storage, other.ram, other.quantity);
    }

    // Getters and setters for each attribute
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getID() {
        return this.ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getProductionYear() {
        return this.productionYear;
    }

    public void setProductionYear(int productionYear) {
        if(productionYear < 1800) {
            throw new IllegalArgumentException("production year should not before 1850");
        }
        this.productionYear = productionYear;
    }

    public int getWarranty() {
        return this.warranty;
    }

    public void setWarranty(int warranty) {
        if(warranty < 0) {
            throw new IllegalArgumentException("warranty time could not be negative");
        }
        this.warranty = warranty;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        if(price <= 0) {
            throw new IllegalArgumentException("price could not be lower than 0 (or set to 0)");
        }
        this.price = price;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getStorage() {
        return this.storage;
    }

    public void setStorage(double storage) {
        if(storage <= 0) {
            throw new IllegalArgumentException("storage could not be lower than 0 (or set to 0)");
        }
        this.storage = storage;
    }

    public double getRam() {
        return this.ram;
    }

    public void setRam(double ram) {
        if(ram <= 0) {
            throw new IllegalArgumentException("ran could not be lower than 0 (or set to 0)");
        }
        this.ram = ram;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        if(quantity < 0) {
            throw new IllegalArgumentException("quantity may not be negative");
        }
        this.quantity = quantity;
    }
    /**
     * Returns a string representation of the Electronics object.
     * @return A string with detailed information about the electronic item.
     */
    public String toString() {
        return 
            "Name: " + this.name + 
            ", ID: " + this.ID + 
            ", Brand: " + this.brand +
            ", Category: " + this.category + 
            ", Year of production: " + this.productionYear + 
            ", Warranty(years): " + this.warranty + 
            ", Price " + this.price +
            ", Description: " + this.description +
            ", Storage(Gb): " + this.storage + 
            ", Ram(Gb): " + this.ram;
    }

    public void addRam(double ram) {
        if((this.ram + ram) > 0) {
            this.ram += ram;
        }else {
            throw new IllegalArgumentException("ram could not be lower than 0 (or set to 0)");
        }        
    }

    public void addStorage(double storage) {
        if((this.storage + storage) > 0) {
            this.storage += storage;
        }else {
            throw new IllegalArgumentException("storage could not be lower than 0 (or set to 0)");
        }
    }
    /**
     * Verifies if the desired quantity of the item is available.
     *
     * @param quantity The desired quantity to check.
     * @return true if the desired quantity is available, false otherwise.
     */
    public boolean verifyQuantity(int quantity) {
        if(quantity > this.quantity) {
            return true;
        }
        return false;
    }
    /**
     * Retrieves a string containing all field values of this Electronics instance.
     * @return A string with all field values.
     */
    public String getAllFields() {
        return 
            getName() + "," +
            getID() + "," + 
            getBrand() + "," + 
            getCategory() + "," + 
            getProductionYear() + "," + 
            getWarranty() + "," + 
            getPrice() + "," + 
            getDescription() + "," + 
            getStorage() + "," + 
            getRam() + "," + 
            getQuantity();
    }
}