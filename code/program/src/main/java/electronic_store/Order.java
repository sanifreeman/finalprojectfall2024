package electronic_store;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
/**
 * Represents an order in an electronic store
 * It implements the SQLData interface to facilitate reading from and writing to a SQL database.
 *
 * @author Tianrui
 */
public class Order implements SQLData{
    private String orderID;
    private String customerID;
    private String productID;
    private int quantity;
    private double price;
    private String typeName = "Order_typ";
    /**
     * Constructor for creating an Order instance.
     *
     * @param orderID The unique identifier for the order.
     * @param customerID The ID of the customer who placed the order.
     * @param productID The ID of the product ordered.
     * @param quantity The quantity of the product ordered.
     * @param price The total price of the order.
     */
    public Order(String orderID, String customerID, String productID, int quantity, double price) {
        this.orderID = orderID;
        this.customerID = customerID;
        this.productID = productID;
        this.quantity = quantity;
        this.price = price;
    }
     // Getters and setters for each attribute
    public String getOrderID() {
        return this.orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getCustomerID() {
        return this.customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getProductID() {
        return this.productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTypeName() {
        return this.typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
    /**
     * Returns a string representation of the Order.
     * @return A string detailing the order's information.
     */
    @Override
    public String toString() {
        return
            " orderID: " + getOrderID() + 
            ", customerID: " + getCustomerID() + 
            ", productID: " + getProductID() + 
            ", quantity: " + getQuantity() + 
            ", price: " + getPrice() + 
            ", typeName: " + getTypeName();
    }
    /**
     * Provides the SQL type name of this object.
     * @return The SQL type name.
     * @throws SQLException If an SQL error occurs.
     */
    @Override
    public String getSQLTypeName() throws SQLException {
        return this.typeName;
    }
    /**
     * Reads data from an SQL input stream to populate the attributes of this Order instance.
     * @param stream The SQLInput stream from which to read.
     * @param typeName The SQL type name of this object.
     * @throws SQLException If an SQL error occurs during reading.
     */
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException{
        setTypeName(typeName);
        setOrderID(stream.readString());
        setCustomerID(stream.readString());
        setProductID(stream.readString());
        setQuantity(stream.readInt());
        setPrice(stream.readDouble());
    }
    /**
     * Writes this Order instance's data to an SQL output stream.
     * @param stream The SQLOutput stream to which to write.
     * @throws SQLException If an SQL error occurs during writing.
     */
    @Override 
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(getOrderID());
        stream.writeString(getCustomerID());
        stream.writeString(getProductID());
        stream.writeInt(getQuantity());
        stream.writeDouble(getPrice());
    }
    /**
     * Retrieves a string containing all field values of this Order instance.
     * @return A string with all field values.
     */
    public String getAllFields() {
        return 
            getOrderID() + "," +
            getCustomerID() + "," +
            getProductID() + "," +
            getQuantity() + "," +
            getPrice();
    }
}