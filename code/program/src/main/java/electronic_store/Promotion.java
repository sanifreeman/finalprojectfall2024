package electronic_store;

public class Promotion {
    private double promotionMultiplier;
    /**
     * Constructor for the Promotion class.
     */
    public Promotion() {
        this.promotionMultiplier = 1;
    }
    /**
     * Gets the current promotion multiplier.
     *
     * @return The current promotion multiplier value.
     */
    public double getPromotionMultiplier() {
        return this.promotionMultiplier;
    }
    /**
     * Tests and applies an applicable promotion based on the customer's characteristics.
     * @param c The customer to test for applicable promotions.
     */
    public void testApplicablePromotion(Customer c) {
        if(c.getID().substring(c.getID().length() - 1).equalsIgnoreCase("s")) { //Student indication
            this.promotionMultiplier = 0.8;
        }else if(c.getAge() < 18) { //Non adult
            this.promotionMultiplier = 0.9;
        }else if(c.getAge() > 60) { //Elderly person
            this.promotionMultiplier = 0.75;
        }
    }
}
